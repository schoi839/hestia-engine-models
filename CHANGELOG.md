# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.51.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.50.0...v0.51.0) (2023-09-12)


### ⚠ BREAKING CHANGES

* **cycle input:** Schema version `23.0.0` is required to run all models.
* **freshwaterEcotoxicityPotentialPaf:** Rename `freshwaterEcotoxicityPotentialPaf` models to
`freshwaterEcotoxicityPotentialCtue`.

### Features

* **cycle input:** ignore background data on Input from Cycle ([6b0b5bd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6b0b5bda2a5ee958d842ce1d2ca37adfcebb2600))


### Bug Fixes

* **freshwaterEcotoxicityPotentialPaf:** update lookup names ([027b221](https://gitlab.com/hestia-earth/hestia-engine-models/commit/027b2217ea4bc26bfc54b3c7fc12ff6c0ae58060))


* **freshwaterEcotoxicityPotentialPaf:** rename `freshwaterEcotoxicityPotentialCtue` ([872cf83](https://gitlab.com/hestia-earth/hestia-engine-models/commit/872cf83c5e62eebd7b19cebab154f5a442fefe63))

## [0.50.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.49.0...v0.50.0) (2023-09-04)


### ⚠ BREAKING CHANGES

* Renamed `orchardBearingDuration` `plantationProductiveLifespan`.
* Renamed `orchardDensity` `plantationDensity`.
* Renamed `orchardDuration` `plantationLifespan`.

### Bug Fixes

* rename `orchard` to `plantation` ([e46d406](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e46d406b30f01f18ff9f10e0aac87a0ade733ae9)), closes [#539](https://gitlab.com/hestia-earth/hestia-engine-models/issues/539) [#531](https://gitlab.com/hestia-earth/hestia-engine-models/issues/531)
* **site:** handle grouped measurements with `values` different length than `dates` ([47e650b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/47e650b5bee669ee81e24c20ab0b72ba70603e95))

## [0.49.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.48.0...v0.49.0) (2023-08-14)


### ⚠ BREAKING CHANGES

* **co2ToAirSoilCarbonStockChangeManagementChange:** Renamed `co2ToAirSoilCarbonStockChangeManagementChange` to
`co2ToAirAboveGroundBiomassStockChangeLandUseChange`.

### Bug Fixes

* **agribalyse2016:** handle operations with string values ([686d3be](https://gitlab.com/hestia-earth/hestia-engine-models/commit/686d3be2aa6df795972006ad2f92c770547b9279))
* **co2ToAirSoilCarbonStockChangeManagementChange:** rename model ([2779b3f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2779b3f79c4b02ed2b8ea53a5b578d7525d1ebc2))
* **economicValueShare:** do not set `100` by default ([ffb7054](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ffb70549eb2dd9531dfd5cd9c749d6e09e48274e))
* **faostat2018 price:** return average when year value is missing ([bfa5cf7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bfa5cf792a9647506d4fed2edf6448d02b7d19d6))
* **haversineFormula:** handle country without centroid ([d0b3c85](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d0b3c85668256564f234102f49bea51b2f3f5393))
* **ipcc2006 belowGroundCropResidue:** use lookup `IPCC_2019_Ratio_BGRes_AGRes` ([9d3db45](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9d3db4586eada7151bd6a89b9debdfd607405d00))

## [0.48.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.5...v0.48.0) (2023-08-02)


### ⚠ BREAKING CHANGES

* **requirements:** Requires schema version `22`.

### Features

* **requirements:** use schema version `22` ([54dfaf5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/54dfaf5d15b88ac9a815dd4143a7e6d032bc59bc))

### [0.47.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.4...v0.47.5) (2023-07-31)

### [0.47.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.3...v0.47.4) (2023-07-31)


### Features

* **ch4ToAirExcreta:** run model if excreta is complete ([07d7a3c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/07d7a3c2ca5186f2559e8807587012af3e4153ab))


### Bug Fixes

* **geospatialDatabase:** fix return object when fetching fails ([aca1ac4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aca1ac433fce661b0cc48fa18c074d4bd3123026))
* **hestiaAggregatedData:** handle no `term` in Input ([092560a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/092560a788bad63d74a62c53289e33886e7da532))

### [0.47.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.2...v0.47.3) (2023-07-11)


### Bug Fixes

* **impact_assessment:** remove nested data on calculation done ([f43d186](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f43d186680a327dc3701f5f124f7c621fb02d3ce))
* **pooreNemecek2018:** average Practice value over multiple products ([da4d211](https://gitlab.com/hestia-earth/hestia-engine-models/commit/da4d2110134b57a81680efbc5ca6db54ca429ebf))
* **site:** run models only if required ([5f62a45](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5f62a45e02f9f7d00b615d4462986fd84f9325b9))

### [0.47.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.1...v0.47.2) (2023-07-03)


### Features

* **cycle practice:** gap-fill default value from lookup ([802826d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/802826de8488bb755b17d46b8dc5448452124ad5))
* **cycle:** add `practice.value` model ([8caadf0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8caadf0a81699aaa37fec9323a24cbce3ffada20))


### Bug Fixes

* handle no `site` on ImpactAssessment ([946f955](https://gitlab.com/hestia-earth/hestia-engine-models/commit/946f955ae37855f3df0724065c6a6bed85fd2847))
* handle old schema for ImpactAssessment product ([3c7fe0d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3c7fe0d1e264857e50170512f5055a8e11028067))
* **hestiaAggregatedData:** handle no `term` on input ([c0c75c1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c0c75c12ba7fdf22ebf94f1bbbe9b886ab3f0d32))
* **ipcc2019 pastureGrass:** handle no grouping for `liveAnimal` ([70c8b75](https://gitlab.com/hestia-earth/hestia-engine-models/commit/70c8b75250446383fc7f7ef61e7cfe7efc39bcd0))

### [0.47.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.47.0...v0.47.1) (2023-06-19)


### Bug Fixes

* **ch4ToAirFloodedRice:** take average of practices factors ([09ad6c7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/09ad6c77b89c973cd1d18dbd39dcb1562f81cdd5))
* **inorganicFertiliser:** use `;` to split values in `mustIncludeId` lookup ([72b7c2b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/72b7c2b39bb5ce0c4797b17cacd27d8e37743566))

## [0.47.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.46.4...v0.47.0) (2023-06-16)


### ⚠ BREAKING CHANGES

* **co2ToAirSoilCarbonStockChange:** Rename `co2ToAirSoilCarbonStockChange` model
`co2ToAirSoilCarbonStockChangeManagementChange`.

### Features

* **transformation:** add new `input.properties` model ([58c2d6c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/58c2d6cef2179cefa233ca80a05bcd2a9ab8551c))


### Bug Fixes

* **co2ToAirSoilCarbonStockChange:** rename `co2ToAirSoilCarbonStockChangeManagementChange` ([4fcf32a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4fcf32a74a2e741f1b93dcdb26d786fdc292e61a))
* **cycle irrigated:** handle units of `% area` ([ddb3854](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ddb38548ece9ca19d940f650f1f53d501a3b804e))
* **ecoinventV3:** fix get impact related to `defaultProperties` without `key` ([e6ca236](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e6ca236aa04e1d9d4d0573b63f00b1c95083d5e8))
* **emissionNotRelevant:** return `Emission` even when present in `Cycle` ([4a08faa](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4a08faa36bb09ee6a86645097fa67a10c70fb783))
* **hestiaAggregatedData:** fix find impact by product term ([081c37b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/081c37bf8b9692978b6f209e39de85075c9f591a))
* **impact assessment:** filter empty values from inputs production ([53cfa6b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/53cfa6b5b58f30386829881952b35c25a8ed0be1))
* **koble2014:** handle all `residueIncorporated` practices ([057053c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/057053c240167cd3ab8ec9ba942ba30e67c143b1))
* restrict product id/termType on any products not just `primary` ([a1bdfa7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a1bdfa7c5de7959920422f0c89a9eeafcb257d1f))
* **transformation input:** fix no matching product from input ([a83a61d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a83a61de94d452739dc9239660c178f71554124b))

### [0.46.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.46.3...v0.46.4) (2023-06-07)


### Features

* **emissionNotRelevant:** only add emissions in Hestia system boundary ([1afdf2f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1afdf2f659789c20632ded65aa0b51649e26dcc9))


### Bug Fixes

* **cycle:** add excreta products even if no `value` provided ([1534b43](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1534b437ca859402d20b577b4ffda6a4e1d9f820))
* **product:** handle failed conversions to unit ([aa1db27](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aa1db27b8be12666decade2747c13e9293a17a1c))

### [0.46.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.46.2...v0.46.3) (2023-06-05)


### Features

* **impact_assessement:** add `product.economicValueShare` model ([129eb5f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/129eb5f5f30836218320524800af696296905c75))


### Bug Fixes

* **impact assessment:** fix get product ([da28d13](https://gitlab.com/hestia-earth/hestia-engine-models/commit/da28d13baf57ee1b0ed732cd481b440cec55c8e3))

### [0.46.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.46.1...v0.46.2) (2023-06-01)


### Bug Fixes

* **utils:** fix `find_by_product` returning random product ([3d04a7f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3d04a7f21e33878053af1cc9faf31e6fd0ea3578))

### [0.46.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.46.0...v0.46.1) (2023-06-01)


### Features

* **transformation:** handle `value`, `min`, `max` and `sd` on all transformations ([add3c0a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/add3c0a867c500554c98c6f5fc7e40ac17bdeec4))

## [0.46.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.45.0...v0.46.0) (2023-06-01)


### ⚠ BREAKING CHANGES

* **impact_assessment:** `product` and `productValue` in `impact_assessment`
have been replaced by `product.value`.
* **requirements:** min schema version is `21.0.0`

### Features

* **requirements:** update schema to `21.0.0` ([264b38b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/264b38b91ffa7c510ff4e5c6dbd58edc86ff0242))


### Bug Fixes

* **impact_assessment:** replace `product` and `productValue` by `product.value` ([8dffbab](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8dffbabf51e6a808dbd2939c36ad1bbba5f95036))

## [0.45.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.7...v0.45.0) (2023-05-19)


### ⚠ BREAKING CHANGES

* Method `emeaEea2019` has been renamed `emepEea2019`.

* rename `emeaEea2019` with `emepEea2019` ([d44de76](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d44de7660fd8a2ea3c5fd54bdb1f2d0456ad3865))

### [0.44.7](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.6...v0.44.7) (2023-05-08)


### Features

* **site:** add `cationExchangeCapacityPerKgSoil` model ([b38ac48](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b38ac48087ed0546a6fc6ddbfbcd1193f12979ab))
* **site:** add `organicCarbonPerHa` model ([ffdaf82](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ffdaf8288d5e113f49149a507f63807c2865691e))

### [0.44.6](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.5...v0.44.6) (2023-04-14)


### Bug Fixes

* **mocking:** fix mocking of function with search param ([0e2127d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0e2127d91427c9c5ab5ba44889bc518a2138fbab))

### [0.44.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.4...v0.44.5) (2023-04-14)


### Bug Fixes

* **mocking:** fix error return single Term as dictionnary ([7cf8ba5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7cf8ba50253b4a045db1735347e4753efe2aac94))

### [0.44.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.3...v0.44.4) (2023-04-14)


### Bug Fixes

* **mocking:** handle mocking of single Term ([dd25e26](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dd25e263cfa2d9ede20401a87be81b8e1b97103c))

### [0.44.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.2...v0.44.3) (2023-04-14)


### Bug Fixes

* **hestiaAggregatedData:** skip when linked impactAssesment is not indexed ([efed13e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/efed13efe7d129b223ac109bea3566111abc13c5))

### [0.44.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.1...v0.44.2) (2023-04-10)


### Bug Fixes

* **ipcc2019 pastureGrass:** handle `meanECHHV` is `0` ([7b212ca](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7b212ca18895d66c60bd6ccad0d96e9ff3fc9334))

### [0.44.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.44.0...v0.44.1) (2023-04-07)

## [0.44.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.43.0...v0.44.0) (2023-03-24)


### ⚠ BREAKING CHANGES

* **requirements:** supported schema version is `18`

### Features

* **ipcc2019 pastureGrass:** require all `animals` blank node have a `value` ([246521d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/246521d7eb83290431925114a7019c21c14057ca))


### Bug Fixes

* **ipcc2019 pastureGrass:** handle `meanDE` is `0` ([9c273f7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9c273f7839c518cd581e3de443b949e3a2dac5aa))


* **requirements:** update schema to version `18` ([31a7737](https://gitlab.com/hestia-earth/hestia-engine-models/commit/31a77376da5ec243790633c6977b88e589571f99))

## [0.43.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.42.0...v0.43.0) (2023-03-15)


### ⚠ BREAKING CHANGES

* min schema version is `17.2.0`

### Features

* **ch4ToAirEntericFermentation:** get feed from `forage` and `liveAquaticSpecies` ([1bc38a9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1bc38a924df08c449261c6bccc419461b694bdbc))
* **cycle:** add `pastureGrass` model ([5cdfeb9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5cdfeb99b1495d7308a61cc2d345ae28b6a7add9))
* **emissionNotRelevant:** set `methodTier` = `not relevant` ([706f742](https://gitlab.com/hestia-earth/hestia-engine-models/commit/706f742b5dbf460e557fe913e56d15d97acb70db))
* **ipcc2019:** add `pastureGrass` model ([40b2c50](https://gitlab.com/hestia-earth/hestia-engine-models/commit/40b2c50f9e623e20e537a60baa39780dea856d12))
* restrict input feed using `fate` field ([3890939](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3890939234806b3acec958dfaf37c63f8cf71682))


### Bug Fixes

* **koble2014:** fix zero division error ([0578916](https://gitlab.com/hestia-earth/hestia-engine-models/commit/05789160a2bd1e8c9d86baa7b8cf98e894999e8f)), closes [#459](https://gitlab.com/hestia-earth/hestia-engine-models/issues/459)

## [0.42.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.41.2...v0.42.0) (2023-02-28)


### ⚠ BREAKING CHANGES

* **faostat2018:** glossary version `0.15` required
* **ipcc2019:** glossary version `0.15` required
* **pribyl2010:** all `pribyl2010` models are now under `site`
* **pooreNemececk2018:** `pooreNemececk2018` measurement models moved to `site`
* **spatial:** all `spatial` models are now under `geospatialDatabase`
* **package:** min schema version is `16`

### Features

* **inorganicFertiliser:** add `min` and `max` from lookups ([67a9507](https://gitlab.com/hestia-earth/hestia-engine-models/commit/67a95071538437ed79d6cc9a4d41dd17099809b6))
* **product:** handle conversion `number` to `lieveweight` ([9b8316f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9b8316fb639c6e90ebcdcb8b3c1544544efe1cae))
* **site:** add bibliography for `pribyl2010` source ([10d41bb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/10d41bb2c7601d9030f2b32d2a6fc91522ba5f46))


### Bug Fixes

* **faostat2018:** fix price for `liveAnimal` ([1ca3f63](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1ca3f63e042ddd095c79a6c51f4cf9b5a761cb09))
* **faostat2018:** update lookup name after glossary update ([e333ffe](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e333ffe32deb8d441d7c8bba504f295c778db866))
* **ipcc2019:** update lookup name after glossary update ([a991c88](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a991c8807835e112d3cca6cf24a2398d3ea330d4))
* **pooreNemececk2018:** move measurement models under `site` module ([a4a2a8b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a4a2a8bb1ef97c3d958a11b0ffde63935b3821b7))
* **pribyl2010:** move models under `site` module ([1e10960](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1e1096019c5480108f202281f46df8ebf6289eef))
* **site:** remove `methodModel` when copying existing `Measurement` ([fa9c8a8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fa9c8a883add9ad4a7c57116f2aa06fae876a6c1))
* **spatial:** move models under `geospatialDatabase` module ([164ba72](https://gitlab.com/hestia-earth/hestia-engine-models/commit/164ba724d9dc9a20aaea2f64dee482176199ea5e))


* **package:** update to schema `16` ([710a47a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/710a47a10d2111fdc025ef60270b939488fe3e5d))

### [0.41.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.41.1...v0.41.2) (2023-02-14)


### Features

* **haversineFormula:** add `transport.distance` model ([4863dd1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4863dd1bc6c9a9b2133ae4a4629cd8b61911e10c))


### Bug Fixes

* **ch4ToAirEntericFermentation:** use new lookup values in `liveAnimal` ([8411ab2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8411ab27c992e635be79f50b31f0d3494f88d813))
* **cycle residueRemoved:** remove usage of `isAboveGroundCropResidueRemoved` lookup ([da4d8db](https://gitlab.com/hestia-earth/hestia-engine-models/commit/da4d8db82f21d739800cc7191af2ef22beb485e1))
* **koble2014 cropResidueManagement:** handle when no total residue ([1b9208d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1b9208d3e536ad52d327359fabd31c15b95afa6f))

### [0.41.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.41.0...v0.41.1) (2023-02-06)


### Features

* **cycle residueRemoved:** handle running from provided products ([3a9519c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3a9519cc404db8c6d661a0da1bc4a3f311c7a720))
* **cycle:** add `residueBurnt` model ([6cffa0e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6cffa0e58e3ebfc1692b6c8113c85e5a66981d13))
* **cycle:** add `residueIncorporated` model ([75475e6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/75475e6a174d5ae349daae5fa0fad2cfc2c5cb14))
* **cycle:** add `residueLeftOnField` model ([ecdf668](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ecdf668021a8b1fc75b17f9b42ee3c346bfeacd4))
* **koble2014:** add new model to rescale added `cropResidueManagement` practices ([f3ae234](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f3ae23476b9e734f34d1b44edcedc7bb3c99b794))


### Bug Fixes

* **inorganicFertiliser:** handle `mustIncludeId` lookup containing multiple values ([04a6e9c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/04a6e9c17717f166b7e5b83db66517059c7bc5ef))

## [0.41.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.40.1...v0.41.0) (2023-01-31)


### ⚠ BREAKING CHANGES

* **faostat2018:** min glossary version is `0.14.0`

### Features

* **ecoinventV3:** handle `pesticideBrandName` mappings using properties ([5aadede](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5aadedee2fb116034d24a69fda33893cc5b8c218))
* **emissionNotRelevant:** add new model to replace default value `0` on first non-required ([da373b4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/da373b416c73176c6d28696ab26cd6c31ae2a668))
* **faostat2018:** use new lookup `region-faostatCroplandArea.csv` ([577c6c5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/577c6c59dcefecc42beb8325eb743ffdfcdef0b8))
* **ipcc2021:** add `gwp100` model ([77097e3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/77097e3a309029b5e3190c4eb55043865a3e9452))


### Bug Fixes

* **ecoClimateZone:** fix bibliography title ([a600362](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a6003627b60025105916cba9ae54fc0c350f2e70))

### [0.40.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.40.0...v0.40.1) (2023-01-16)


### Features

* **akagiEtAl2011AndIpcc2006:** account for `discardedCropBurnt` ([ab1ae3d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ab1ae3d5d0f3427b532421b0b94490185630aad4))
* **deRuijterEtAl2010:** account for discarded crop residue ([f08ddd8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f08ddd849c8a2cc2ccb9dfa23f6a430d7d6bf995))
* **ipcc2019:** account for discarded crop on field ([50d8ed3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/50d8ed38bcc8d597a15624e8694089950c0b1a3a))


### Bug Fixes

* **faostat2018 price:** get price for Cycle year if available ([c0f7a00](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c0f7a00a0f643b31130cfbb1c98a7787d4374b32))

## [0.40.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.39.1...v0.40.0) (2023-01-02)


### ⚠ BREAKING CHANGES

* **spatial:** min version of optional `hestia_earth.earth_engine` is `0.2.0`

### Features

* **croppingIntensity:** only run on non-orchard crops ([b901978](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b901978903796f408c1c55acf602b2e3dcc278a8))
* **longFallowRatio:** only run on non-orchard crops ([496df7c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/496df7c052df2518cae6a9d4e64d00bac2f1ba2a))
* **measurement:** add restriction run by model ([bed1166](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bed11667b725815786fd31a412f862bc15ae6dc8))
* **utils:** improve logging on pesticide data from lookups ([aa98977](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aa98977327a157bf6b346419eb570a9ac2be7fdc))


### Bug Fixes

* handle download method not found ([608b61b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/608b61bc7136931fa7b1684f7833a37f7d9ba830))
* **spatial:** update bibliography title for HWSD source ([f072d0d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f072d0dbc5d70fad835ee4125fd82c0c711c29a7))


* **spatial:** migrate to `hestia_earth.earth_engine` version `0.2.0` ([62c73d3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/62c73d39dfb54318e725a27b79e2b5ea94eecb7d))

### [0.39.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.39.0...v0.39.1) (2022-12-13)


### Features

* **transformation:** add `input.value` model ([28f43a0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/28f43a07a753f86a3cdf9b89497d645338be3ee7))


### Bug Fixes

* **excreta:** handle products without `value` ([6ab56f8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6ab56f873f70d4093cad9ca0e11cbee9277e293d))

## [0.39.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.38.0...v0.39.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **requirements:** min schema version `14`

### Features

* **requirements:** update schema to `14` ([fb9f162](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fb9f16203fe674a101a249fe066810edad48d939))

## [0.38.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.37.0...v0.38.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **spatial:** `fallowCorrection` renamed to `longFallowRatio`
min glossary version is `0.12.0`

### Features

* **environmentalFootprintV3:** add `freshwaterEcotoxicityPotentialPaf` model ([6a8631c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6a8631cd47c3d986b69b7e881bbe5112c6ddae9c))


### Bug Fixes

* **hyde32:** fix lookup value for 100 years ([bf0bd9b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bf0bd9b0ef74d33d9df8f4e673978db770b32c14))


* **spatial:** rename `fallowCorrection` to `longFallowRatio` ([84ff71b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/84ff71b027e01f04b125016e8adc8abbe31426e7))

## [0.37.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.36.2...v0.37.0) (2022-11-22)


### ⚠ BREAKING CHANGES

* `landOccupation` split in `DuringCycle` and `InputsProduction` models
`freshwaterWithdrawals` split in `DuringCycle` and `InputsProduction` models
`landTransformation` split in `DuringCycle` and `InputsProduction` models
* **ecoinvent:** ecoinventV3 data file is no longer included.
If you wish to use the `ecoinventV3` model, please reach out to
community@hestia.earth to download the data file. Thank you

### Features

* **economicValueShare:** handle complete product sum above 80% ([b08d27f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b08d27fc53d9ff57fd19b1dd28f143ae9a850c84))
* **model links:** add terms from `fuelElectricity` model ([ad1971f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ad1971f1818e1d16db906f9189bbfa48e118a3df))


### Bug Fixes

* **hyde32:** set value to `0` if land occupation is `0` ([e2ccf47](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e2ccf474c45ce809a995650b3b25e600bc772a55))
* **impact assessment:** sum up inputs production and cycle value for next cycle ([58625a8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/58625a86043212d5c82acb18de49e0239ea149cb)), closes [#397](https://gitlab.com/hestia-earth/hestia-engine-models/issues/397)
* **input properties:** handle linked impact not found ([1c0c0c4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c0c0c4a82c6fc0ffa4dbf7d823ccd8276e9b437))
* remove `statsDefinition` for `Indicator` with `value` only ([1ead716](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1ead7164e842657429213936b294a6091e85484a))


* **ecoinvent:** remove data file ([b29e633](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b29e633089bdb45a3c488defdaa2f11a6e093c8a))
* split `InputsProduction` and `DuringCycle` models ([2181b24](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2181b241eb954495f1c35db10d42b5ab1ab3c1f8))

### [0.36.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.36.1...v0.36.2) (2022-11-15)


### Features

* **links:** add excreta models in `pooreNemecek2018` ([606909d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/606909d8a5230f0736e2a759aeb05ad43a20ef56))


### Bug Fixes

* **fuelElectricity:** run only if data is incomplete ([fe67767](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fe677675dc8ebaa23c6ff0f530cc4ab5788d9b11))
* return `0` when no nitrogen content but data complete ([ffc6178](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ffc617890a3b97114d394afcbeb71c4fceac3d17))

### [0.36.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.36.0...v0.36.1) (2022-11-09)


### Bug Fixes

* **spatial:** fix models using Cycle instead of Site ([4c76002](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4c76002aee1c360908b9f30a179155fbb5995502))

## [0.36.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.35.1...v0.36.0) (2022-11-08)


### ⚠ BREAKING CHANGES

* **spatial rainfall:** `rainfallAnnual` replaced with `precipicationAnnual` under `spatial` model
* **croppingIntensity:** `croppingIntensity` is added on Cycle `practices`
* **fallowCorrection:** `fallowCorrection` is added on Cycle `practices`

### Features

* **emeaEea2019:** skip adding `0` value for `Transformation` complete fuel ([5bc4d0f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5bc4d0f4c495342d32d729ff75dadd16ea8877b4))
* **model links:** add `methodTier` when applicable ([2fe5f82](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2fe5f82cefe913e47095a8224fd69807fb9c263a))


### Bug Fixes

* **croppingIntensity:** set as Cycle Practice ([a199947](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a1999477e72960edd31e15757d2b63406df6529b))
* **cycle irrigated:** handle `water` completeness ([9375009](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9375009b4e98f7b349433e5c1093e66379fbee8c))
* **fallowCorrection:** set as Cycle Practice ([a21cc43](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a21cc4396deb474b83ed10ba9aa96b7d8c042ca3))
* **transformation excreta:** use existing product in case it's a different `[@id](https://gitlab.com/id)` ([058f12c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/058f12ca92096da7cd96ce7bfbbed57e83034c90))


* **spatial rainfall:** rename to precipitation ([3bc8a7f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3bc8a7f4e8b88d6d82e7530e3d3b16cf54aed556))

### [0.35.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.35.0...v0.35.1) (2022-10-18)


### Bug Fixes

* **spatial:** handle both `region` and `country` for measurement fallback ([3d3c735](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3d3c7353fd69f620cc6203f8f7301cdbc1b3ac0e))

## [0.35.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.34.1...v0.35.0) (2022-10-10)


### ⚠ BREAKING CHANGES

* all models with `fertilizer` renamed to `fertiliser` and
min schema version is `12.0.0`

### Bug Fixes

* **blank node:** handle `run required` on multiple terms models ([ef90040](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ef900401f6b94ca97fb86f767957a9e802c87e35))
* **inorganicFertilizer:** run if term is present but has no `value` ([86a768a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/86a768a62f1f3842714788142d83de5effdef711))
* **inorganicFertilizer:** skip running if terms are not missing ([e6a4d5a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e6a4d5ad03bc7e4d086ecf0d45066acf780c68ff))
* **property:** handle could not download term for properties ([62f0ae6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/62f0ae6046a4a048beeb6aa4dce3ad32aa3a30e9))


* rename `fertilizer` to `fertiliser` ([0287b85](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0287b85b5d38daf3785da184429d6765226432ea))

### [0.34.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.34.0...v0.34.1) (2022-10-04)


### Features

* **cycle:** add `inorganicFertilizer` model ([d6b6eec](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d6b6eec7ca9888760dad985ddb6ca8e74b7fcc5d))
* **impact assessment:** add `productValue` model ([0f66c17](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0f66c17893128d1643592eb656fd15a937eaefa8))
* **ipcc2019:** add `no3ToGroundwaterCropResidueDecomposition` model ([6f0d19c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6f0d19cc1af43d04865325c526d825f698ea24a2))
* **ipcc2019:** add `no3ToGroundwaterExcreta` model ([96eaba3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/96eaba35574a80804cf91f564df574cfaf366177))
* **ipcc2019:** add `no3ToGroundwaterInorganicFertilizer` model ([44c1656](https://gitlab.com/hestia-earth/hestia-engine-models/commit/44c1656305e9b99fc0160c217f5802a804f20b6b))
* **ipcc2019:** add `no3ToGroundwaterOrganicFertilizer` model ([1d41ba1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1d41ba1657907ec75f7a0fddea77ad500cf71d51))


### Bug Fixes

* **faostat2018:** use `primaryMeatProductFAO` to get `price` for `liveAnimal` ([a2e18e1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a2e18e1ee3807c3821b29bd41f70991c85579985))
* **pooreNemecek2018:** require all feed to specify property or fail model ([8e06bd3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8e06bd3de2b2891148306633c0ee5d74ef35695d))
* **pooreNemeck2018:** do not add negative product on mass balance items ([26fe020](https://gitlab.com/hestia-earth/hestia-engine-models/commit/26fe020f2ce64fb024b77da9bc6434eefa72db5f))

## [0.34.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.33.0...v0.34.0) (2022-09-20)


### ⚠ BREAKING CHANGES

* **transformation post_checks:** `transformation`/`post_checks` moved to `transformation`/`product.excreta`
* **transformation excreta:** `transformation`/`excreta` moved to `transformation`/`input.excreta`
* **requirements:** min schema version supported is `11.0.0`
* **cycle:** renamed `transformations` model to `transformation`
* **ch4ToAirExcreta:** requires min version of Glossary `0.9.0`

### Features

* **cycle:** add `excretaKgMass` model ([6b616d0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6b616d03bcdb9eac2cf459769f216059782bc38c))
* **cycle:** add `excretaKgN` model ([ee0feac](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ee0feac004f1492b0a0bf8d94bc9c18be2d564d8))
* **cycle:** add `excretaKgVs` model ([4d4fe08](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4d4fe08411cbf5884f9d9dd11431822a31fc0542))
* **dataCompleteness:** handle `glass or high accessible cover` site type ([3db88af](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3db88af66f1bc38489185015b8689baa9250f073))
* **scarcityWeightedWaterUse:** handle `glass or high accessible cover` site type ([0e74bbf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0e74bbfcbc53a045f1922a93cdc4f39ab3ae703d))
* **transformation product:** gap-fill excreta `kg VS` same value as `Input` ([77fbe0b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/77fbe0bbb4a71c58494d0da5f8b7acdb51115322))
* **transformation:** add `excreta` model ([eaba826](https://gitlab.com/hestia-earth/hestia-engine-models/commit/eaba8267b8a1e58fe3b821fd6c47965f17aa39fb))


### Bug Fixes

* **ch4ToAirExcreta:** account for `kg VS` units only ([56bf309](https://gitlab.com/hestia-earth/hestia-engine-models/commit/56bf3093f4dbcc3f270efb6eabd2909b42a2611b))
* **ch4ToAirExcreta:** rename Duration from `month_x` to `x_months` ([cc60e54](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cc60e54dddda83609be1cb4cc4791fb25c7267e1))
* **cycle irrigated:** set `0` when not irrigated and `100` when irrigated ([97baeda](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97baeda3d39dd525e04a1839240c5d954375181b))
* **impact assessment irrigated:** handle `value` above `0` for irrigated ([c8e39cd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c8e39cda617c9e300cc257ed819b6216a25e5649))
* **nh3ToAirExcreta:** get NH3 factor per excreta `kg N` Input ([1b3dcaf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1b3dcaf3a48662abd91f4dca382900ca569ab13a))
* only account for `kg N` when calculating total TAN content ([3242555](https://gitlab.com/hestia-earth/hestia-engine-models/commit/324255593799c5938f7b3b6fb161038aeb062530))
* **price:** update lookup name ([f43b3e2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f43b3e2c18584ffa193533e57ea52b24c81a0c67))
* **spatial:** add max size requirement for models with lookup fallback ([1cb9bf1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1cb9bf1c69b42979f06d0df35ccda583e168d743))


* **cycle:** move `transformations` to `transformation` to align with model name ([6f47d67](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6f47d67aed5c6432b3a1b2884bc0f70ced148386))
* **requirements:** require schema min version `11.0.0` ([7420a3f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7420a3f85f4753f9ab5b7eb146203903cc3fe38d))
* **transformation excreta:** move to `input.excreta` ([38b5481](https://gitlab.com/hestia-earth/hestia-engine-models/commit/38b5481e6a2ca3ec9d7db597f19f8145610f7272))
* **transformation post_checks:** move to `product.excreta` ([0101a62](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0101a627cd85460cb80cc8daad5d74ec5b0f6bde))

## [0.33.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.32.2...v0.33.0) (2022-09-06)


### ⚠ BREAKING CHANGES

* min glossary version is `0.8`
- `carcassWeight` renamed to `coldCarcassWeight`
- `dressedCarcassWeight` renamed to `coldDressedCarcassWeight`

### Features

* **mode links:** add `ecoinventLinks` ([312e790](https://gitlab.com/hestia-earth/hestia-engine-models/commit/312e790f32c226e3699294b023ee53461f62a18a))


### Bug Fixes

* **emissions:** fail copy emissions if `economicValueShare` is `0` ([bb43995](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bb43995bfd3e4e737865c355331c01ad8c466e95))
* **freshwaterWithdrawalsDuringCycle:** do not add if `economicValueShare` is `0` ([3e9ec91](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3e9ec91270201f3ddf3f62d121405e065c561553))
* **ozoneDepletionPotential:** remove requirement on `pesticideAi` ([1a4b587](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1a4b587264c59678e4236fd949b78ad71f0858eb))
* **recipe2016Hierarchist:** fix wrong lookup for `ozoneDepletionPotential` ([85b5ac7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/85b5ac76042311b891357ffec9c582c5d55b1e70))


* renamed carcass weight in cold carcass weight ([d6d6ce5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d6d6ce51c0abd07c7b8faf90bd12f67d171e0ef3))

### [0.32.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.32.1...v0.32.2) (2022-08-23)


### Features

* **ch4ToAirEntericFermentation:** get default digestibility from lookup ([e7f507f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e7f507f2c292f290e8c10e87ddffa2ff69bcf49c))
* **ch4ToAirEntericFermentation:** only run if 80% of feed has nutrient content ([adc7351](https://gitlab.com/hestia-earth/hestia-engine-models/commit/adc73514bd3086e5aab352b274406d3c5e3acea2)), closes [#340](https://gitlab.com/hestia-earth/hestia-engine-models/issues/340)
* **concentrateFeed:** gap-fill `energyDigestibility` properties ([a5963ec](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a5963ec271b6f3945b1e0980bba8721aa62c88f1))
* **concentrateFeed:** run if 80% of properties are specified ([f664585](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f664585590d756da68f4753bfbfde86dcdc16087))
* **cycle:** add `carcassWeightPerHead` model ([f6d074f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f6d074f262729faeb351f7d4792184ab0af4ae09))
* **cycle:** add `dressedCarcassWeightPerHead` model ([ad9c803](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ad9c803b987eab0fbd8100fb8b0ae01fcb94e664))
* **cycle:** add `readyToCookWeightPerHead` model ([39b169b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/39b169b3a5518fed53f14e245c68eb3cef761315))
* **liveAnimal:** only run for `animal housing` and `permanent pasture` sites ([df996f8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/df996f83a66f98d64559195fa9f259f4e5a235f9))
* **lookup:** handle value as matching between model and value ([e544c69](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e544c693531231aa72bbdb220da74e852c2240f4))


### Bug Fixes

* **aboveGroundCropResidueTotal:** handle `aboveGroundCropResidueRemoved` is `0` ([b0fa229](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b0fa229102c52e47fe1fb25c5d1d65a401900a8b))
* **ch4ToAirEntericFermentation:** do not run if DE or NDF is missing ([a50d07c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a50d07cfb8bf1189d10ee76575a5a4f83a805991))
* **ch4ToAirEntericFermentation:** read from `properties` on `Input` ([65d67f8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/65d67f8dad60b86aa35ca09dcb727a9b60a8dd61))
* **cycle:** do not gap-fill per head properties if already present ([a4d399f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a4d399ff582fc102751ca604df7efcdaf64fe09d))
* **hestiaAggregatedData:** skip `Input` if also a `Product` ([74d12d1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/74d12d1eeb1d98ec1f6441f919b9275a44cb4869))
* **liveAnimal:** use the conversion in the same `units` ([3c84330](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3c8433015873d4782b14b2ee05e9936dd99296b7))
* **nh3ToAirInorganicFertilizer:** fallback to unspecified `kg N` ([2aec58d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2aec58dd5a2232a5e84213a51661e105b78b43e3))

### [0.32.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.32.0...v0.32.1) (2022-08-10)


### Features

* **ecoinventV3:** group emissions by `inputs` and `operation` ([46a757b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/46a757b9ca413accc33d80c72b8dfba2a44fda97))
* **linkedImpactAssessment:** group emissions by `inputs` and `operation` ([75e3114](https://gitlab.com/hestia-earth/hestia-engine-models/commit/75e31149286407ec1cf487b5c262302729afbfe2))


### Bug Fixes

* **fuelElectricity:** group by `operation` ([98cfd70](https://gitlab.com/hestia-earth/hestia-engine-models/commit/98cfd70f470c464ca8cd0e098cb924e1aac2e540))

## [0.32.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.31.0...v0.32.0) (2022-08-05)


### ⚠ BREAKING CHANGES

* **koble2014:** replaced `koble2014`/`residue` by
`residueBurnt`, `residueRemoved` and `residueLeftOnField`

### Features

* **emission:** handle run required for emission/model mapping ([040a64e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/040a64ed4f2fcbe6a5f69321cc23f8879f9af52c))


### Bug Fixes

* do not double-count cycle emissions ([25bc21e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/25bc21e1bba2effb194b750b80cc85abac845fca))
* **fuelElectricity:** do not group inputs by `operation` ([909ee08](https://gitlab.com/hestia-earth/hestia-engine-models/commit/909ee084e9c7f67b3560972209356035c3ec0e37))
* **spatial:** do not return `0` if value could not be calculated ([e2ee976](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e2ee97609c39ec6b00dc551e8dfe6a09c3f17c43))
* **spatial:** remove area size restriction on models with fallbacks to region lookup ([dda36bf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dda36bfe2e525e57901e6a4d07cf2ff28d5ecf66))


* **koble2014:** split residue models in 3 models ([17f6621](https://gitlab.com/hestia-earth/hestia-engine-models/commit/17f66212c632de5e33ddce5e3aa40ca1a3adcf7b))

## [0.31.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.30.2...v0.31.0) (2022-08-01)


### ⚠ BREAKING CHANGES

* **pooreNemecek2018:** now requiring version `0.6.0` of the glossary lookups

### Features

* **requirements:** add method to return model returned schema ([cc1d129](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cc1d129f8950d73c9371a025f15d8a4e4ba55574))
* **spatial:** add fallback to region-level measurement ([7071ee7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7071ee7b5cebb5a0bb9a2267d02e9e2f12ffa9c0))


### Bug Fixes

* account for completeness when calculating impacts from `pesticideAI` and `fuel` ([c3e5d6f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c3e5d6f5962d3043a4a5f1cfc63c6f15de096fbd))
* **impact assessment:** set `cycle.site` only if exists ([f2fb3ef](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f2fb3efa8a83afb127b32e956a5cccaf938786d9))
* **model-links:** include missing `ecoinventV3` ([cb4346b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cb4346b69dfd9bd064f4af08b91467e73081e088))
* **pooreNemecek2018:** rename `excreta` and `excretaVs` lookups ([02a131f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/02a131fc3aff964decfbeb30c26482e5ebd372ab))
* **site:** handle date without year and month ([0ddf3c1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0ddf3c13ee7b5f7ab50bfbb4f0132ddbeaf48d01))

### [0.30.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.30.1...v0.30.2) (2022-07-26)


### Bug Fixes

* **concentrateFeed:** handle no primary product ([9908c51](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9908c51a8ab0a3ae3bd3922208b1e774904556ba))

### [0.30.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.30.0...v0.30.1) (2022-07-26)


### Features

* **cycle:** add `input.properties` model ([5f1868e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5f1868e7f9884a0d084e494b6ac445676dbc16e5))
* **cycle:** add model for `concentrateFeedBlend` and `concentrateFeedUnspecified` ([e99adcc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e99adcc1b4a13672bbf3c912042114672c788272))
* **ecoinventV3:** add `operation` if set on `input` ([189f432](https://gitlab.com/hestia-earth/hestia-engine-models/commit/189f43263af103c5dbe8c93d8bdc8e34d5a778fd))
* **emissions:** copy `operation` if set on `emission` ([9643f39](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9643f39eeb50e353ad249c1d0a156e95e48291ad))
* **impact assessment:** include `transformation` into `emissionsResourceUse` ([be406b0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/be406b0ff994d42e2d097df0bdaf351cdf692782))
* **linkedImpactAssessment:** add `operation` if set on `input` ([aaa2aae](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aaa2aaee4eb7258bacd18db6b9a853722e7bcb07))
* **site:** add `potentialEvapotranspirationAnnual` and `potentialEvapotranspirationMonthly` ([acad606](https://gitlab.com/hestia-earth/hestia-engine-models/commit/acad606fe55009a174de213ebbb5cafbab9e154b))
* **site:** add `precipitationAnnual` and `precipitationMonthly` models ([0c062c9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0c062c969f1449535b2f9eec8f90403c76cb9c35))
* **site:** add `rainfallAnnual` and `rainfallMonthly` models ([f7d82bf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f7d82bf74c267a76b5341b8700204012deab95d5))
* **site:** add `temperatureAnnual` and `temperatureMonthly` models ([05492ae](https://gitlab.com/hestia-earth/hestia-engine-models/commit/05492ae625071de4acca51dc5bd74945d5b424d7))
* **transformation:** handle product with `kg` units ([97efa67](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97efa67152cc6f4fdf3cb7b3bc8e27a332bc1377))


### Bug Fixes

* **ch4ToAirExcreta:** get CH4 B0 factor per `excreta` Input rather than Product ([97a661c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97a661cc7c8c061c4cb0db86ac054949aa3793a7))
* **emissions:** remove empty fields when adding 0 emission ([9da81d6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9da81d6df40fb8961affd0cd0577cb48d690b630))
* **excretaKgN:** get nitrogen content from `liveAniaml` in `head` unit ([4ab0a1c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4ab0a1cc180d523f05e351867d098e39c70b0e05))
* fix `should_run` log when pesticides values are required ([ac12480](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ac12480cc144a106a12eab79678cedd88032f95a))
* **impact assessments:** account for unrelated method when computing endpoint ([adf93cd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/adf93cd25e8edec8752aa624afc6bc90736657e4))
* **input:** handle `kg Mass` excreta as nitrogen ([b7400b3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b7400b3ffb83107186b52e7ba1ccfe0498c87d10))
* **nh3ToAirExcreta:** get total of TAN from `kg`, `kg N` and `kg Vs` excreta ([e98ca71](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e98ca719516e6f918a271d69cce650aed678cb6c))
* **price:** fix wrong unit for `liveAnimal` price ([2d482a2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2d482a2bb1e6bbe837e208658d9ecda5a3309170))

## [0.30.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.29.0...v0.30.0) (2022-07-15)


### ⚠ BREAKING CHANGES

* **chaudharyEtAl2015:** all `chaudharyEtAl2015` models have been removed

### Features

* **damageToFreshwaterEcosystemsClimateChange:** add lcImpact models ([6d3bfe3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6d3bfe31384bf1ef9d055217a14b79bbb5dca4ba))
* **damageToFreshwaterEcosystemsFreshwaterEcotoxicity:** add lcImpact models ([2302dd0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2302dd020d75a2b26b7732019426420302c5608c))
* **damageToFreshwaterEcosystemsFreshwaterEutrophication:** add lcImpact models ([ab68a58](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ab68a5877d446747cc12f00916f524b0ea83b115))
* **damageToFreshwaterEcosystemsPdfYear:** add lcImpact models ([c9ec89b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c9ec89bbd677438bd30c83956110719f73f1a991))
* **damageToFreshwaterEcosystemsSpeciesYear:** add ReCiPe models ([48ba2f7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/48ba2f7d5a1b6b4ecdbdab937a6efdffb6c4a145))
* **damageToFreshwaterEcosystemsWaterStress:** add lcImpact models ([b90aff2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b90aff230072a0fb36f881b4a3f9b8594f134f5a))
* **damageToHumanHealth:** add lcImpact models ([a8bbd61](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a8bbd61f05f46f31e408d7fd6b7747962179f215))
* **damageToHumanHealth:** add ReCiPe models ([2418f38](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2418f3897773b8334eb42d760ba15e8f83e024da))
* **damageToHumanHealthClimateChange:** add lcImpact models ([1a602aa](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1a602aa8c3632fe7d091addc4c70813b4aabdc70))
* **damageToHumanHealthHumanToxicityCancerogenic:** add lcImpact models ([5f83f11](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5f83f11ae0e4699734f11bafaada1a523b1abd9c))
* **damageToHumanHealthHumanToxicityNonCancerogenic:** add lcImpact models ([ae76f60](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ae76f60777844a1e9c831dd79421e002dceeb3e4))
* **damageToHumanHealthParticulateMatterFormation:** add lcImpact models ([e9c21c5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e9c21c5ab3e3fcbc05d3ec479252b0b481cdd4d2))
* **damageToHumanHealthPhotochemicalOzoneFormation:** add lcImpact models ([8afb555](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8afb5558c3329cb420427601038478112b58ebcb))
* **damageToHumanHealthStratosphericOzoneDepletion:** add lcImpact models ([caacfe3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/caacfe33ff4eeb6e2a1ebd22a1af084c3b67537d))
* **damageToHumanHealthWaterStress:** add lcImpact models ([e5b0498](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e5b0498831b33280cefbfbe677878b6ce027147d))
* **damageToMarineEcosystemsMarineEcotoxicity:** add lcImpact models ([13ad530](https://gitlab.com/hestia-earth/hestia-engine-models/commit/13ad5300f9fb729f32e80e256dc5fa1fdf161280))
* **damageToMarineEcosystemsMarineEutrophication:** add lcImpact models ([96985a2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/96985a2d6492eb3f858599e160580200f394ff7e))
* **damageToMarineEcosystemsPdfYear:** add lcImpact models ([72d23fa](https://gitlab.com/hestia-earth/hestia-engine-models/commit/72d23fab32a7336849244590b4da8248bc7f4026))
* **damageToMarineEcosystemsSpeciesYear:** add ReCiPe models ([7eac270](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7eac270863d11c9f9a560a07bfe511edd964d70f))
* **damageToResourceAvailability:** add ReCiPe models ([75b00ed](https://gitlab.com/hestia-earth/hestia-engine-models/commit/75b00ed785613ff1a502920a9e4cd7502415b6ec))
* **damageToTerrestrialEcosystemsClimateChange:** add lcImpact models ([2a769a9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2a769a9f75886eb0cc22808ed1351ccef78338b5))
* **damageToTerrestrialEcosystemsLandTransformation:** add `chaudharyBrooks2018` model ([b428fe7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b428fe7ef4b13c57818dd1b91c65e1c2f9a7111c))
* **damageToTerrestrialEcosystemsLandTransformation:** add `chaudharyBrooks2018` model ([c79f102](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c79f1024c81640d8c2b2d617043fd099383393ce))
* **damageToTerrestrialEcosystemsPdfYear:** add lcImpact models ([bb38d1b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bb38d1bf9147e2acff3c38a204ce14f56a1d3dba))
* **damageToTerrestrialEcosystemsPhotochemicalOzoneFormation:** add lcImpact models ([90676de](https://gitlab.com/hestia-earth/hestia-engine-models/commit/90676deb26c724efe683ad76b172ad5b6a6f7f69))
* **damageToTerrestrialEcosystemsSpeciesYear:** add ReCiPe models ([6736669](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6736669ce38bdcd861168c96d349943b944fb1a5))
* **damageToTerrestrialEcosystemsTerrestrialAcidification:** add lcImpact models ([d0674e8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d0674e89a575735fc0a38dabf069c96bd320a445))
* **damageToTerrestrialEcosystemsTerrestrialEcotoxicity:** add lcImpact models ([9214f49](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9214f49e8e6b10b0e4a2cfedf020b83a7b004338))
* **damageToTerrestrialEcosystemsTotalLandUseEffects:** add `chaudharyBrooks2018` model ([fc698e6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fc698e6ce61607f51226834df75719be4ea64da4))


### Bug Fixes

* **chaudharyEtAl2015:** remove models ([3fac94f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3fac94f3af1956961b835eb7cb0837ac3eb9f196))
* **ecoinventV3:** group identical `inputs` under the same emissions ([903e52f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/903e52fc26f8f8b1c3753707914af7603089635a))
* **linkedImpactAssessment:** handle `Indicator` with no `value` ([ed3cc48](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ed3cc48929881051df5179660f77c0600c7dba2c))
* **model-links:** fix incorect `modelKey` ([cc5caf5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cc5caf572518526b23f4cbb14072619391aacfaf))
* **nh3ToAirInorganicFertilizer:** account for `fertilizer` completeness correclty ([9a500c6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9a500c60353519b61f547c167ca378988178fddc))

## [0.29.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.28.0...v0.29.0) (2022-07-11)


### ⚠ BREAKING CHANGES

* **spatial:** `phosphorusPerKgSoil` renamed to `totalPhosphorusPerKgSoil`

### Features

* **spatial:** rename `phosphorusPerKgSoil` to `totalPhosphorusPerKgSoil` ([836ccb9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/836ccb93fef7ae353bc8b469bdc36336bf776383))


### Bug Fixes

* **ch4ToAirEntericFermentation:** only set `sd` if available ([93d2f73](https://gitlab.com/hestia-earth/hestia-engine-models/commit/93d2f7366b17f91c88a5c51fcb0cd1c1d86fc78e))
* **pribyl2010:** omit `description` from copied measurement ([48e0f83](https://gitlab.com/hestia-earth/hestia-engine-models/commit/48e0f835c20054082c0036d324e25f6f64377a25)), closes [#302](https://gitlab.com/hestia-earth/hestia-engine-models/issues/302)
* **pToSurfaceWaterSoilFlux:** run when `slope` is `0%` ([f3796e7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f3796e770bd8b9bf49ab9720a0d2e946d4a38c12)), closes [#301](https://gitlab.com/hestia-earth/hestia-engine-models/issues/301)

## [0.28.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.27.4...v0.28.0) (2022-07-04)


### ⚠ BREAKING CHANGES

* **otherBackgroundDatabase:** `otherBackgroundDatabase` is now `linkedImpactAssessment`
* **requirements:** requirements are returned as a list of nodes

### Features

* **feedConversionRatio:** skip run practice from lookup ([3c7e224](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3c7e224d8a6c93dae4bc24556ae2624f37aedb38))
* **hestiaAggregatedData:** search by `input.region` if specified ([a313b2a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a313b2add613ce2eaa515181597803c4ab105e2c))
* **koble2014:** handle restrictions on practices ([42c46b8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/42c46b8840be2bccf12959708008fd09463c63ff))
* **requirements:** find requirements from requirements ([830edf3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/830edf37d6b2c32619981f048b186ee0e163fecd))
* set `Emission` to `0` when model not required to run ([cf83c0c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cf83c0c5c1d4baeba64d7154f6856317df162ec7))
* **spatial:** improve search existing using nested `measurements` ([45c74da](https://gitlab.com/hestia-earth/hestia-engine-models/commit/45c74da525b4a2ea14898f05f2f8eea7fb48f71f))


### Bug Fixes

* **emission:** skip adding `0` value `Emission` for `Transformation` ([01d2b4d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/01d2b4d4e53b1f5675a574d5d391177e2e5a6ad6))
* **feedConversionRatio:** only run if cycle has feed ([40709c2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/40709c25639baee218d9f9fc691ff6f6e911ad14))
* **spatial:** handle no `endDate` in find measurement ([c0a4bf7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c0a4bf789b03716f77be458572116bc4f7874bba))
* **transformation:** link with `transformation` not `inputs` ([8b33a33](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8b33a33167c9cca0361a8636fd9e7e01293b3fdf))


* **otherBackgroundDatabase:** rename to `linkedImpactAssessment` ([d26d7b3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d26d7b35658ef70367ddf6a163855fec13b2980d))

### [0.27.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.27.3...v0.27.4) (2022-06-21)


### Bug Fixes

* **ch4ToAirExcreta:** use `volatileSolidsContent` on `excreta` in `kg N` ([5568ccf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5568ccf944154a4cb0992c6929574e200bd3c9f1))
* **requirements:** fix termType=Completeness should return nothing ([f3b016e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f3b016e8772bda60babe4fb57bae633886d7e95f))

### [0.27.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.27.2...v0.27.3) (2022-06-06)


### Features

* **n2OToAirCropResidueDecompositionDirect:** handle factors for wet/dry zone ([4ece081](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4ece081e42d43ada78454254a201bc49292026d4))


### Bug Fixes

* **requirements:** remove empty values from `get_term_ids` ([99919ad](https://gitlab.com/hestia-earth/hestia-engine-models/commit/99919ad734bc486234f9dfcf6100aa2c27a27d31))

### [0.27.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.27.1...v0.27.2) (2022-06-06)


### Features

* **ecoClimateZone:** add `description` with name of zone ([dc02c30](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dc02c307ab8d9b798483e45a1b12c3ead3e62013))
* **ipcc2019:** add `n2OToAirCropResidueDecompositionDirect` model ([1c3abe4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c3abe478e4cea134209ca49d8abf77870aab22a))
* **requirements:** add function to return list of term IDs ([004b70a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/004b70ad408e0ee867bc0a60c208970f6d92963f))


### Bug Fixes

* **recipe2016:** do not run if pesticides factors are missing ([5339d6d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5339d6d905e57d96682e17d977262ece183e8bed))

### [0.27.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.27.0...v0.27.1) (2022-06-02)


### Bug Fixes

* add `dataCompleteness.cropResidue` on emissions ([2084521](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2084521970c6e1e222aa4913ee2e51c44a40c024))
* **stehfestBouwman2006GisImplementation:** add excreta N total ([c0b8eb2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c0b8eb2c99ffe41b6c72e7889528f2c70a5f38ec))
* **stehfestBouwman2006:** take into account all nitrogen for every model ([f4b99f1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f4b99f1b897bf0dbc20ff93eb17cd96137df9a51))

## [0.27.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.26.0...v0.27.0) (2022-06-02)


### ⚠ BREAKING CHANGES

* **requirements:** install `hestia_earth.earth_engine` if using the `spatial` model

### Features

* **requirements:** add function to list models only ([5b90c7d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5b90c7d30280bfbc656c368b66f40afee7af1377))
* **requirements:** filter by `termType`, term `[@id](https://gitlab.com/id)` and `siteType` ([0b0d23b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0b0d23bf7ea589bb7d8af2186dfd0c578c6792af))
* **requirements:** filter non-useful blank nodes ([3ec65b0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3ec65b0d2174eff1f5087023f13e4a57f2120856))
* **requirements:** return requirements by `model` or `tier` ([7c99d63](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7c99d638f5a802d825f6d7c302c9537ecaffe851))
* **usetoxV2:** add `freshwaterEcotoxicityPotentialPaf` model ([b8e4c6c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b8e4c6c668c2923983e1bf2b33686917785a2b19))


### Bug Fixes

* **hestiaAggregatedData:** skip inputs if `skipAggregation` is set ([ff00438](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ff00438d71f4464b9c802c2a96dfd60920ec9ca6))
* **hyde32:** handle `endDate` as a full date ([62b65f9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/62b65f9e7444d4e82b68b532f42b905cc25b6f13))


* **requirements:** remove `hestia_earth.earth_engine` dependency ([c925367](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c9253679fbc18c0a1339f6c098dfe37e343e3a07))

## [0.26.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.25.3...v0.26.0) (2022-05-23)


### ⚠ BREAKING CHANGES

* **pooreNemeck2018:** `organicFertilizerToKgOrMass` model has been removed

### Features

* **impact-assessment emissions:** do not group emissions by `term.[@id](https://gitlab.com/id)` ([7a1ea07](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7a1ea07c37a3f355a04dcd6ed61ea75e589a63a3))


### Bug Fixes

* **hyde32:** handle no `siteType` when retrieving factor ([9b8c71d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9b8c71da2a49c305a9ba9121d591a47b4c4f82bd))
* **pooreNemeck2018:** remove deprecated `organicFertilizerToKgOrMass` model ([5416b77](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5416b7743cdf8d228c01b2a2856e1c918388dfbe))

### [0.25.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.25.2...v0.25.3) (2022-05-13)


### Features

* **faostat2018:** add `landTransformationFromCropland100YearAverage` model ([2b57122](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2b571227f55df1d78a82d6c8b69b6556a817782d))
* **faostat2018:** add `landTransformationFromCropland20YearAverage` model ([46120b8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/46120b858f843fee645193071b517553062fb661))
* **hyde32:** add land transformation models ([854b88e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/854b88e94191e50053b3e550116702be50e9d4fe))
* **pribyl2010:** add new organic carbon to matter models ([5f0e034](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5f0e03450c69644c7106cae051dec6a349d73aad))


### Bug Fixes

* **ch4ToAirExcreta:** default to `high` productivity ([a741b98](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a741b9863ac410bae137ffbffb55a370eb8affd9))
* **utils:** load recalculated data of input -> impact if available ([7e5aac1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7e5aac199f6e00f62c80d9ab2b3426bf10ff7e55))

### [0.25.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.25.1...v0.25.2) (2022-04-23)


### Features

* **cycle:** add `energyContentLowerHeatingValue` model ([47f1d14](https://gitlab.com/hestia-earth/hestia-engine-models/commit/47f1d144c7f3dc7acb3f0d82b90e602c1cd0c42a))
* **recipe2016Egalitarian:** add `ecosystemDamageOzoneFormation` model ([5e32a39](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5e32a3984c19f5f0637d2d527ef9a81d39288b61))
* **recipe2016Egalitarian:** add `fossilResourceScarcity` model ([659a064](https://gitlab.com/hestia-earth/hestia-engine-models/commit/659a064f91a405eac7921eb7de54d7d6030c63ae))
* **recipe2016Egalitarian:** add `freshwaterAquaticEcotoxicityPotential14Dcbeq` model ([d85e649](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d85e649ea66b0b332a631e2b7352b17fc8c0f04a))
* **recipe2016Egalitarian:** add `humanCarcinogenicToxicity` model ([a6a644a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a6a644a6c2219ce26e5e17c7c37fc4b1154490d4))
* **recipe2016Egalitarian:** add `humanDamageOzoneFormation` model ([26312b5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/26312b53205b680bb857ef99502063bb51adee2a))
* **recipe2016Egalitarian:** add `humanNonCarcinogenicToxicity` model ([d9582a1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d9582a116578ca563b9b610194350cce974d431b))
* **recipe2016Egalitarian:** add `marineAquaticEcotoxicityPotential14Dcbeq` model ([08618a0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/08618a01d30cfc6ce2448e41e8325bdb9560f832))
* **recipe2016Egalitarian:** add `ozoneDepletionPotential` model ([8992d0c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8992d0c861731ee8d35e353efc57834f1b73b6f9))
* **recipe2016Egalitarian:** add `terrestrialEcotoxicityPotential14Dcbeq` model ([1530535](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1530535936cb268918cf9bdcfbefbf37e85ab57f))
* **recipe2016Hierarchist:** add `ecosystemDamageOzoneFormation` model ([51884b5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/51884b5d72472a4cb684ad9a7deeffeafb873b57))
* **recipe2016Hierarchist:** add `fossilResourceScarcity` model ([535e355](https://gitlab.com/hestia-earth/hestia-engine-models/commit/535e3553186e3a22af640f6168f4bd0d82c9fb8d))
* **recipe2016Hierarchist:** add `freshwaterAquaticEcotoxicityPotential14Dcbeq` model ([9b593cf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9b593cfd16fd8ba4e88fc2a64947ac5599f13d0b))
* **recipe2016Hierarchist:** add `humanCarcinogenicToxicity` model ([17e6f81](https://gitlab.com/hestia-earth/hestia-engine-models/commit/17e6f814a0afb803270a7d7ebe90b78eee482ca7))
* **recipe2016Hierarchist:** add `humanDamageOzoneFormation` model ([1cd0c87](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1cd0c87a440a3527d8d6a82dd221b8f98431ed71))
* **recipe2016Hierarchist:** add `humanNonCarcinogenicToxicity` model ([c75e227](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c75e22755adfb8ca51a0e1b5bdca38e4d8b385f4))
* **recipe2016Hierarchist:** add `marineAquaticEcotoxicityPotential14Dcbeq` model ([d60626f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d60626f6f83b4fef2435086f6d58caa64b725559))
* **recipe2016Hierarchist:** add `ozoneDepletionPotential` model ([e61b343](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e61b3437e0db1cb234ccd896da7f6ca01073eb12))
* **recipe2016Hierarchist:** add `terrestrialEcotoxicityPotential14Dcbeq` model ([f4dba62](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f4dba62c7b7d5b513e86de05987d8dd728a44561))
* **recipe2016Individualist:** add `ecosystemDamageOzoneFormation` model ([5c1eb1c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5c1eb1c5beee48f00e58eacb9030932fa81ea7d8))
* **recipe2016Individualist:** add `fossilResourceScarcity` model ([c88a77d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c88a77dc0db69c0645b3ac3ead4d12f64cc811af))
* **recipe2016Individualist:** add `freshwaterAquaticEcotoxicityPotential14Dcbeq` model ([b89562a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b89562a70def2e3e4d082be3bfbc5de0e41d8111))
* **recipe2016Individualist:** add `humanCarcinogenicToxicity` model ([fe4c8f1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fe4c8f1b23d284099786e74c1d448bb96917da7a))
* **recipe2016Individualist:** add `humanDamageOzoneFormation` model ([8996c2b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8996c2b76b584da6757040c3d0f68f0549958a4c))
* **recipe2016Individualist:** add `humanNonCarcinogenicToxicity` model ([1c1b3cc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c1b3ccf22dbbce9c000054117fb15014a70a002))
* **recipe2016Individualist:** add `marineAquaticEcotoxicityPotential14Dcbeq` model ([3ac35fe](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3ac35feec03de15862f20eee42b369515ef9ab3e))
* **recipe2016Individualist:** add `ozoneDepletionPotential` model ([bd601d7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bd601d749f3f1a23febf795fc6fb7dae719e5712))
* **recipe2016Individualist:** add `terrestrialEcotoxicityPotential14Dcbeq` model ([0de635e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0de635eed6fc8945623c467570d429237a257125))


### Bug Fixes

* **data ecoinventV3:** fix incorrect term `[@id](https://gitlab.com/id)` ([168fbeb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/168fbebac32f2eba55b65b7bc6998ccfc5db3057))
* **model-links:** fix `impact_assessment` model name ([1caa21b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1caa21bf1e856e97905bfef619e09f8d0491e27f))
* **nitrogenContent:** add missing log on run by product ([9acacdc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9acacdc8ff8170dd7c2ffe943889e24920ef6139))
* **rootingDepth:** only run on `crop` ([26ea0ea](https://gitlab.com/hestia-earth/hestia-engine-models/commit/26ea0ea58c87e9c72ef42b6be1be1e2c87abd336))
* **stehfestBouwman2006:** handle `OverflowError` on high yield ([062046f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/062046f0238130267d0034f5261a605a7864de4e))

### [0.25.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.25.0...v0.25.1) (2022-04-09)


### Bug Fixes

* **koble2014:** add missing `shouldRun` log on `residueLeftOnField` ([0502c9d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0502c9d34a59724e960dfaced008718fb07a052b))

## [0.25.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.11...v0.25.0) (2022-04-06)


### ⚠ BREAKING CHANGES

* **clayContent:** `sandContent` and `siltContent` removed from `clayContent`

### Features

* **spatial:** add `potentialEvapotranspirationLongTermAnnualMean` model ([8b8fe25](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8b8fe25c812287a71f695b62c4979d9304f03984))
* **spatial:** add `rainfallLongTermAnnualMean` model ([84e4ace](https://gitlab.com/hestia-earth/hestia-engine-models/commit/84e4acefb6bcff8aca66afc868a20b214c7d596a))
* **spatial:** add `temperatureLongTermAnnualMean` model ([c9c6acc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c9c6acc7eeef44b36a653b63c67ced680e034776))


* **clayContent:** split into 3 different models ([8282bb0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8282bb09874cd6849ef77692a5711185a2f1565d))

### [0.24.11](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.10...v0.24.11) (2022-04-05)


### Features

* **spatial:** add `potentialEvapotranspirationAnnual` model ([97de336](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97de336953eb77f6bfbe5127e5f19e33cef95b75))


### Bug Fixes

* **impact emissions:** sum up emissions with same `term` ([9d60347](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9d60347ed02d1a29c7a317262a67b971effeae4f))
* make sure evs and revenue are `0` if product value is `0` ([7205111](https://gitlab.com/hestia-earth/hestia-engine-models/commit/72051118b92463fa7031827e1f58d9c0a4354463))
* **nitrogenContent:** set logs on products instead of property ([993db54](https://gitlab.com/hestia-earth/hestia-engine-models/commit/993db542f093d7c3216bf5778982ced7f889caab))
* **residueLeftOnField:** add missing should_run log ([bf60c3b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bf60c3b2ea1d88bb6cf5d622078e2ed9fcc95d1c))

### [0.24.10](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.9...v0.24.10) (2022-03-28)


### Features

* **organic:** only check for `standardsLabels` practices ([a0cc2f2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a0cc2f22a6b8c763791878662ddf4849fbf71ea9))


### Bug Fixes

* **koble2014:** set crop residue to `0` if practice is `0` ([e41f96e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e41f96e347a43c27aa64e99f65cd2a1f19ec2e44))
* **residueRemoved:** only run when `dataCompleteness.cropResidue` is false ([92da3e4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/92da3e48ca4ea5c27e11bfc395bd8d6db35ace4c))

### [0.24.9](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.8...v0.24.9) (2022-03-21)


### Features

* **clayContent:** run when there is no `value` ([1c83f68](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c83f681bc43bd609d168c3a1b7fdbbf8bd293ab))

### [0.24.8](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.7...v0.24.8) (2022-03-14)


### Bug Fixes

* **currency:** handle unsupported currencies ([f0f8645](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f0f8645eb16927e751f3b01c732668603048ef57))

### [0.24.7](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.6...v0.24.7) (2022-03-11)


### Features

* **cycle:** add `residueRemoved` model ([9ecf181](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9ecf18136d15a14ddc23bdfb29fac330f989b681))

### [0.24.6](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.5...v0.24.6) (2022-03-09)


### Bug Fixes

* **clayContent:** round value to 2 decimals ([8dd0945](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8dd0945ad58eef55ae90fadc80b9abad9673ff01))
* **ecoClimateZone:** round result to closest integer ([42a467e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/42a467ebf1cdfb7b8085517dc58703a73093f64e))

### [0.24.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.4...v0.24.5) (2022-03-08)


### Features

* **dataCompleness other:** set to `True` for orchard crop if `saplings` is present ([2f411be](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2f411be49b8b212a0d9c40956a1afd8150173896))
* **price:** handle `liveAnimal` ([70dd3c9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/70dd3c976b6335e33168fc353f91345fbb265778))


### Bug Fixes

* **economicValueShare:** handle complete/incomplete cases ([0d558d3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0d558d32773a0ee8dee480b62043161513235cde))

### [0.24.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.3...v0.24.4) (2022-02-23)


### Bug Fixes

* **log:** disable steam handler completely ([02b8275](https://gitlab.com/hestia-earth/hestia-engine-models/commit/02b827570320c5dbdca776eb0f5a127f833617cf))

### [0.24.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.2...v0.24.3) (2022-02-23)


### Features

* **log:** use `LOG_TO_STREAM` to disable logging to stream (enabled by default) ([edcc543](https://gitlab.com/hestia-earth/hestia-engine-models/commit/edcc5435330770ea6f295676deb2616aaf434a12))

### [0.24.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.1...v0.24.2) (2022-02-23)


### Features

* **hestiaAggregatedData:** log should_run on model key ([0c8a812](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0c8a812ef2bad0ed8e81d05bc9130ca22aec099a))
* **log:** expose stream handler ([79ca254](https://gitlab.com/hestia-earth/hestia-engine-models/commit/79ca254e60323e7ff60d3ea1ad9301c7508c90ae))
* **price:** set to `0` if economic value share lookup is `0` ([db6c549](https://gitlab.com/hestia-earth/hestia-engine-models/commit/db6c54983e6e333a7160c50a686dc29b399d4e03))
* **product:** add `currency` model ([b74b7fd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b74b7fd3c8b69d1b6ee0c3d07f7109e16e625ee2))
* **revenue:** set to `0` if price is `0` even without a `value` ([2e97103](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2e97103d33b464c64dc45634d82026e6e62a0bd3))


### Bug Fixes

* **price:** set default `currency` as USD ([6e52ee4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6e52ee4755932d6137db76be5b003d624e3a2f49))

### [0.24.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.24.0...v0.24.1) (2022-02-21)


### Bug Fixes

* does not return InputsProduction indicator if no value ([5fcfdd4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5fcfdd435427ff80788e8bd0ac77f9328e4d3ebe))

## [0.24.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.23.1...v0.24.0) (2022-02-14)


### ⚠ BREAKING CHANGES

* **pooreNemecek2018:** rename all `AquaculturePonds` models to `AquacultureSystems`

### Bug Fixes

* **models:** set info level for requirements ([8f8f1ad](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8f8f1ad00434e8f9ab618be6106508994b8304af))


* **pooreNemecek2018:** rename aquaculture ponds to systems ([e400c94](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e400c94c81db986f2f108496fb039bd731237eaf))

### [0.23.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.23.0...v0.23.1) (2022-02-11)


### Bug Fixes

* **requirements:** require utils `0.10.10` minimum ([ac5274f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ac5274fde9cb6dfb5f0a6dceb397c29ceb7ee452))

## [0.23.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.22.4...v0.23.0) (2022-02-11)


### ⚠ BREAKING CHANGES

* **spatial:** access to earth_engine features require Google Cloud Account.
See README for more information.

### Features

* **log:** set level as error for required data to run models ([9df6206](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9df6206ddd2faa5207dae3ad1a360bfcc94fb51e))


### Bug Fixes

* **cycle:** do not fallback to `fallowCorrection` for `orchard` crops ([17f0e7d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/17f0e7ddcb961134141229ca7d4fa6337f649625))
* **spatial:** skip `ecoregion` and `aware` by region ([686a5b9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/686a5b9c99d0c4a05e5e39e87536ed35eea59fcf))
* **utils:** handle indicator without a value ([55131b0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/55131b01e0037218015fb26363e68080977a0d93))


* **spatial:** use `earth_engine` library instead of API ([08c2d8c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/08c2d8cbbc9cf04b4e33fb4163972cc64786b433))

### [0.22.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.22.3...v0.22.4) (2022-02-09)


### Features

* **mocking:** add function to mock API search results ([2378b64](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2378b64e029629d2891407c0f0f874cbe64ca684))


### Bug Fixes

* **impact_assessment:** add missing pre/post checks ([354c8a1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/354c8a15a45cb524c55887d7997cbe158aad0672))
* **landOccupation:** skip Cycle value if requirements not met ([9042097](https://gitlab.com/hestia-earth/hestia-engine-models/commit/904209748d37826509a13951634df9df9761d575))

### [0.22.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.22.2...v0.22.3) (2022-02-08)


### Bug Fixes

* **price:** fix error in default value ([564bd9b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/564bd9b73da2487d2ca7cbcd3d72fd4752b55f52))

### [0.22.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.22.1...v0.22.2) (2022-02-08)


### Features

* **price:** handle `animalProduct` ([5ff53d1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5ff53d19db2ba2651018223e53319890d8e0db75))


### Bug Fixes

* fix calculation of land occupation ([d0796d8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d0796d846d5c51b445037c1228b40ae0a4949954))

### [0.22.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.22.0...v0.22.1) (2022-02-07)


### Bug Fixes

* **economicValueShare:** handle products without `revenue` ([3f0c1f8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3f0c1f850da8fdbf36f6674f6df9915a34c2ddb4))

## [0.22.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.21.1...v0.22.0) (2022-02-04)


### ⚠ BREAKING CHANGES

* **aggregated:** `input.aggregated` renamed to `input.hestiaAggregatedData`

### Features

* **otherBackgroundDatabase:** set model to Hestia for aggregated impact ([91178a9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/91178a9165f73c354cf095381b29728204091821))


### Bug Fixes

* **economicValueShare:** do not overwrite when value is present ([69c05e6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/69c05e68684d79ba791009043b854e0cd5a7bff9))
* **economicValueShare:** skip single products without share lookup ([4c5d5dc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4c5d5dc3cbcd9f0473fed99165f4c8fa7e9b7db8))
* **ipcc2006:** fix land occupation per hectare on soil cultivation models ([255d982](https://gitlab.com/hestia-earth/hestia-engine-models/commit/255d98271fe672c8c838951d0e6a5061cbbe2110))
* **log:** change log level to `WARN` for lookup tables ([fdf5a01](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fdf5a013e2d32f63f9f76a239c9c0ffe8dafbb5f))


* **aggregated:** rename to `hestiaAggregatedData` ([566a231](https://gitlab.com/hestia-earth/hestia-engine-models/commit/566a231d9c7c269984b33b9689ddb90694ca9086))

### [0.21.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.21.0...v0.21.1) (2022-02-02)


### Bug Fixes

* **log:** log to DEBUG by default but override for stream handler ([6259141](https://gitlab.com/hestia-earth/hestia-engine-models/commit/625914141e9178fbc6576ae6ec4f6c973c93021a))

## [0.21.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.20.1...v0.21.0) (2022-01-23)


### ⚠ BREAKING CHANGES

* **eutrophicationPotentialExcludingFate:** `eutrophicationPotentialExcludingFate` moved to `cml2001Baseline`
* **terrestrialAcidificationPotentialIncludingFateAverageEurope:** `terrestrialAcidificationPotentialIncludingFateAverageEurope`
moved to `cml2001Baseline`

### Bug Fixes

* **eutrophicationPotentialExcludingFate:** change model ([7dda32e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7dda32e01b5e64d20b14a4f3800413837b44ad70))
* **terrestrialAcidificationPotentialIncludingFateAverageEurope:** change model ([791d445](https://gitlab.com/hestia-earth/hestia-engine-models/commit/791d44546645a8040e48bfb508349dac67ec87a6))

### [0.20.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.20.0...v0.20.1) (2022-01-12)


### Features

* **emissions:** return `null` value if product yield is `0` ([5312bb9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5312bb99a10192b91225af85bb15bf948090bf4f))
* **revenue:** handle case where product yield is `0` ([6968dbd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6968dbd86989838afffcc56360a4ed2577dc8043))


### Bug Fixes

* **economicValueShare:** skip unset `currency` and `revenue` ([831e8a1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/831e8a108f41b2261a34718ae27841772788c4e0))
* handle `null` values in all `Indicator` models ([b3751d2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b3751d28d35dc85a210bad1b3d37f7f595220cd6))
* handle multiplication of None values ([71d238b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/71d238b7d82a769974467f357bcc11d8fbd56abf))
* **landTransformationFromForest20YearAverage:** use land occupation per m2 ([d78d64a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d78d64acd878b6ee1ee13ff3ac545d174221b30c))

## [0.20.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.19.2...v0.20.0) (2021-12-15)


### ⚠ BREAKING CHANGES

* **recipe2017:** `recipe2017` models is split in 3 models:
- `recipe2016Egalitarian`
- `recipe2016Hierarchist`
- `recipe2016Individualist`

### Bug Fixes

* handle no inputs value in `characterisedIndicator` ([6e4aba0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6e4aba0de87e1ac9a60d84dabc2eccb1438cec66))


* **recipe2017:** split into 3 perspectives ([97b7080](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97b7080ca46942b8c1c094ba31a9abf95e0fc450))

### [0.19.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.19.1...v0.19.2) (2021-12-03)


### Bug Fixes

* **nErosionSoilFlux:** handle no `slope` value ([8bdf177](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8bdf17740ef59f3ac5e8ebb69cbb2e1cbec224c7))

### [0.19.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.19.0...v0.19.1) (2021-12-03)


### Features

* **recipe2017:** add `freshwaterEutrophicationPotential` model ([1fd9415](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1fd941506fe4c087509b559b3166ae3c3fe8b75d))
* **recipe2017:** add `marineEutrophicationPotential` model ([04063a9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/04063a9aea2f1137ae75eb23b88f02294259d29f))
* **recipe2017:** add `terrestrialAcidificationPotential` model ([47bf902](https://gitlab.com/hestia-earth/hestia-engine-models/commit/47bf902263dfc45fa412df3367382e31ab50853e))


### Bug Fixes

* **impacts:** only sum up emissions that have a factor ([f43b5ac](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f43b5ac9e368786162bc23c3f82f556fcef3a406))

## [0.19.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.18.3...v0.19.0) (2021-11-06)


### ⚠ BREAKING CHANGES

* **crop:** current version requires version `0.0.12` of glossary

* **crop:** replace `cropGroupingFAO` grouping columns with new names ([c035f22](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c035f22d862ceaeb621955ed8b0ac776415d6d20))

### [0.18.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.18.2...v0.18.3) (2021-11-02)


### Features

* **log:** log missing lookup as `error` ([b6be606](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b6be606fdec66000550e8a5fd12090cc138331fb))


### Bug Fixes

* **biodiversityLossLandTransformation:** fix wrong terms ([f01de85](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f01de85e85bb488fe29f557054114012eb924444))
* **scarcityWeightedWaterUse:** handle no scarcity `factor` ([2f847de](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2f847de139ce1c517d9076e3d997dd5a9514b2e1))

### [0.18.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.18.1...v0.18.2) (2021-11-02)


### Features

* log missing lookup factors ([36c0454](https://gitlab.com/hestia-earth/hestia-engine-models/commit/36c0454cf40632d852b376e2b51f5046cc95406b))

### [0.18.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.18.0...v0.18.1) (2021-10-29)


### Features

* **fuelElectricity:** add `operation` used for input ([0c2b323](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0c2b323240f3b47bfe1eda833a2fa9cf329f2a2f))


### Bug Fixes

* **spatial aware:** skip run on GADM due to small precision ([f98fb10](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f98fb10fa728b7182cd53019ce5b02edd5ea615f))
* **spatial ecoregion:** skip run on GADM due to small precision ([90b2458](https://gitlab.com/hestia-earth/hestia-engine-models/commit/90b245824f489a2b12a4f5a7a99ca186c3624db4))

## [0.18.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.15...v0.18.0) (2021-10-25)


### ⚠ BREAKING CHANGES

* **landTransformationFromForest20YearAverage:** `landTransformationFromForest20YearAverage` model called on `ImpactAssessment`

### Features

* **cml2001NonBaseline:** account for input `impacts` ([d9eec0e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d9eec0edc4d972432c857231da391f696b49d18e))
* **croppingIntensity:** round value to 7 digits ([143e2b8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/143e2b8ac3c95c7ab2b2a8b58010406e65b67ac0))
* **cycle dataCompleteness:** add `animalFeed` model ([e03af69](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e03af69153bc696a94e0b4af31b0a7dbca7c59a2))
* **cycle dataCompleteness:** add `excretaManagement` model ([46dc093](https://gitlab.com/hestia-earth/hestia-engine-models/commit/46dc093df43a9b45ee8c6709cc8304e97274c17d))
* **cycle dataCompleteness:** add `material` model ([01038df](https://gitlab.com/hestia-earth/hestia-engine-models/commit/01038df1e8bcd1cd8fb69c3481cfa68f32965866))
* **cycle dataCompleteness:** add `other` model ([510e649](https://gitlab.com/hestia-earth/hestia-engine-models/commit/510e649109eedcd4f110477c699ab28dc2e65808))
* **economicValueShare:** handle products with `0` share ([5d4d770](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5d4d770f3f6544692b3a62b9810369ef6d352ae2))
* **gwp100:** account for input `impacts` ([17a8f82](https://gitlab.com/hestia-earth/hestia-engine-models/commit/17a8f82ee5001d546312e8bf28fee98eb35dcbd1))


### Bug Fixes

* **biodiversityLossLandTransformation:** handle no transformations ([b89542f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b89542f7dbaf167d048b35aef9c738b2a9aab966))
* **chaudharyEtAl2015:** always run model and take inputs into account ([c38f58f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c38f58f33ecdb62706b520def85a56c176c0dfbe))
* **chaudharyEtAl2015:** require a `site` ([0388979](https://gitlab.com/hestia-earth/hestia-engine-models/commit/03889798cf82882c79dca2de2f01f57db3bb32c4))
* **cycle input aggregated:** require primary product to run seed ([dd16f88](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dd16f8846f317f369fe8078285c0c2c76113e5ad))
* **feedConversionRatio:** remove `siteType` restriction ([11da0e0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/11da0e0c233f4ee11b83292ce40105c2cc721448))
* handle impact without a cycle product ([72e4003](https://gitlab.com/hestia-earth/hestia-engine-models/commit/72e4003bf653cd30bd5a73760e2feacfb7e59006))
* **scarcityWeightedWaterUse:** always run take into account `inputs` ([60c225c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/60c225ccea3ba02b1e61745ba57e9e6db663d56e))
* **scarcityWeightedWaterUse:** require a `site` ([01d0162](https://gitlab.com/hestia-earth/hestia-engine-models/commit/01d01620c27b89d5d355a7ba613dc4ca1b71d6e0))


* **landTransformationFromForest20YearAverage:** split into cycle and inputs term ([2489011](https://gitlab.com/hestia-earth/hestia-engine-models/commit/24890116b48751ca149a597ee8040e12e13e3fa4))

### [0.17.15](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.14...v0.17.15) (2021-10-13)


### Bug Fixes

* **cycle transformations:** handle no `transformations` in cycle ([219949e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/219949e94c6bec625daeb2601a7b330cbd0b6a8f))

### [0.17.14](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.13...v0.17.14) (2021-10-13)

### [0.17.13](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.12...v0.17.13) (2021-10-13)


### Bug Fixes

* **cycle post_checks:** add missing `transformations` model call ([1484a70](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1484a700cf088db0f6c6ca7a3e764bd3f609b67e))

### [0.17.12](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.11...v0.17.12) (2021-10-13)


### Features

* **cycle post_checks:** add `transformations` model ([e824291](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e82429123decce25490f81cd8f9ce1d685eb45dc))


### Bug Fixes

* **nitrogenContent:** handle no total ([1b3dfb2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1b3dfb2d7fcfd41a40bba7cdeaa4bcc79d5f739c))

### [0.17.11](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.10...v0.17.11) (2021-10-11)


### Features

* **slope:** round value to 7 digits ([4d58f76](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4d58f76d883c132c38d3bf38223448114936b21e))


### Bug Fixes

* **nitrogenContent:** fix error no yield ratio ([af0a0e4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/af0a0e4d670b2ef2d30f01e91e9fe63a75a6dd24))
* **nitrogenContent:** handle multiple crops no lookup value ([cbac622](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cbac622f585a30febf04ce3a19c03ed76da5ee95))

### [0.17.10](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.9...v0.17.10) (2021-10-08)


### Features

* **economicValueShare:** set to 100% for a single Product ([a4293a2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a4293a2920f9c89990ab89ac2fad7a967e25a349))


### Bug Fixes

* **economicValueShare:** run by `revenue` with same `currency` only ([be3a57c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/be3a57c15241f46d8ced5e9926f4ccc54b8f8c41))
* **economicValueShare:** skip required `revenue` for `cropResidue` and `excreta` ([fc584dd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fc584dd4ad2fd442401674dbc4ee6a1b6bac4207))
* **nh3ToAirCropResidueDecomposition:** remove restriction nitrogen content too low ([d377e44](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d377e44080df4169775c21f7352d0dd11ad541d4))
* **spatial soilPh:** round to `7` decimals ([23497d5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/23497d5814c3134ccd59af4bf39e6a3122f9d988))

### [0.17.9](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.8...v0.17.9) (2021-10-06)


### Bug Fixes

* **ipcc2019 nitrogenContent:** handle single crop with CR nitrogen content ([66222b7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/66222b7cd51ce985512de389cb11827531a2d423))

### [0.17.8](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.7...v0.17.8) (2021-10-06)


### Features

* **land occupation:** calculate for orchard crops ([4d14cb8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4d14cb8f5434ea1844eef905d492a416a26af1f2))
* **pooreNemecek2018:** add `longFallowPeriod` model ([b4e154b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b4e154b732f9a6157aa69be4814a9b6f8ffe90c4))
* **pooreNemecek2018:** add `orchardBearingDuration` model ([d987295](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d9872953cf0eac5500e801dae65eb5e73328fd0c))
* **pooreNemecek2018:** add `rotationDuration` model ([c1e18d5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c1e18d55ad50222e04cd8568073e99d2a5fb6aef))


### Bug Fixes

* **biodiversityLossLandTransformation:** run when emissions are `0` ([7556898](https://gitlab.com/hestia-earth/hestia-engine-models/commit/755689806b43b7e065ba2d8389bf5c0f30f38fdd))
* **impact assessment:** fix conversion GADM-ID to level 1 ([16a1c51](https://gitlab.com/hestia-earth/hestia-engine-models/commit/16a1c51a3f1463e72bd6985e18506366d1875f75))

### [0.17.7](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.6...v0.17.7) (2021-10-05)


### Features

* **blonkConsultants2016:** add `landTransformationFromForest20YearAverage` model ([d4ff2fb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d4ff2fb345c61345d56481fae9b24fa77443cdd1))
* require min schema version `6.8.0` ([98e8e21](https://gitlab.com/hestia-earth/hestia-engine-models/commit/98e8e21d4cd5f9792a8d2e107aa84d102cb19c92))

### [0.17.6](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.5...v0.17.6) (2021-10-01)


### Features

* **spatial:** override default max area size with env variable ([981b5c8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/981b5c87263e754a4e310354afe411ea5fba9c0d))

### [0.17.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.4...v0.17.5) (2021-09-29)


### Bug Fixes

* **ecoinventV3:** group emissions for the same Input ([2c20afd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2c20afd1d2529b4c46faf0d99fa8e39d501ada95))
* **freshwaterWithdrawals:** do not multiply inputs value by 1000 ([eaa7bf4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/eaa7bf4903b4a00ceeb57ad99d3da1dcde954fc6))
* **ipcc2006:** set default value when `no3` term is unset ([072717d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/072717dbd6ef3f94f765ff316d95b475934c011f))
* **nitrogenContent:** fallback load dryMatter from default properties ([8ff94f4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8ff94f4194ed5cb616f4d6d4e83accd8eaccb9bc))

### [0.17.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.3...v0.17.4) (2021-09-24)


### Bug Fixes

* **cycle utils:** handle no rooting depth max value ([791ad5f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/791ad5f9b4ce82a463c5b64449c530380a341fcb))

### [0.17.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.2...v0.17.3) (2021-09-24)


### Features

* **cycle:** add `aboveGroundCropResidueTotal` model ([70f5fd0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/70f5fd053a180c79c5270b0816930a8d6ea1a0ad))
* **economicValueShare:** use revenue if provided or default to crop lookup ([9a5a118](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9a5a118374ead479a872c05682ec263c51414d13))
* **ipcc2019:** add `aboveGroundCropResidueTotal` model ([71a3b8c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/71a3b8c7cea87caf77f5a29de4b388310df1cfde))
* **ipcc2019:** add `belowGroundCropResidue` model ([5a9a508](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5a9a508c30c6ca716d563b98623472956eb46974))
* **nitrogenContent:** handle multiple `crop` products ([25b441e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/25b441ef0a7d3f135e3c65f617d6bc3f977e0bec))
* **pooreNemecek2018:** add `aboveGroundCropResidueTotal` model ([0f7b450](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0f7b4503713eea2fbf11a0c257c8f0c8ba0705ff))
* **pooreNemecek2018:** add `belowGroundCropResidue` model ([789ca0f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/789ca0f126dfd320be1b51f28afe955526d10817))


### Bug Fixes

* **ipcc2006 aboveGroundCropResidueTotal:** run only with 1 crop product ([3bb8542](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3bb85428634e19675235b4727ed38dc582bf472a))
* **nh3ToAirCropResidueDecomposition:** return `0` when abg nitrogen content <= 5 ([f949e34](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f949e343b0b1c7f474cc4406ccf79a6c34639c34))
* **pooreNemecek2018:** use max rooting depth instead of average ([b91e7e3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b91e7e3c5056dc97930b225b1d3aa45665e7a9d6))

### [0.17.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.1...v0.17.2) (2021-09-21)


### Features

* **agribalyse2016:** add `fuelElectricity` model ([f708b02](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f708b0233e4206375306e11965187149f2fd00f1))
* **cycle:** add `cycleDuration` model ([fe6e18b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fe6e18b3b365951d7f71616420e4a435e966d304))


### Bug Fixes

* **cropppingDuration:** only run with `riceGrainPaddy` or `ricePlantsPaddy` ([75f2171](https://gitlab.com/hestia-earth/hestia-engine-models/commit/75f21719663bcaa686c6b848ea6d2c72a61c8468))
* **cycle:** handle `Input`/`Product` without `[@type](https://gitlab.com/type)` ([3b62310](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3b623101831e75f10c73cfce9689e9f3b949ef3f))
* **freshwaterWithdrawals:** handle no yield ([3cc42ef](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3cc42ef9a8dbeaf90d26bdcf0864d9f5a0759769))
* **nh3ToAirExcreta:** run with no excreta but complete ([efb3991](https://gitlab.com/hestia-earth/hestia-engine-models/commit/efb39914793cf799e14add71c0bc881684a3cda4))
* **scarcityWeightedWaterUse:** run model if fresh water is `0` ([dd24473](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dd24473834ad32f47e8e00ad0fbcbc166a994463))

### [0.17.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.17.0...v0.17.1) (2021-09-20)


### Features

* **freshwaterWithdrawals:** account for conveyancing efficiency ([87e527c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/87e527cfbce1dac98ac04b2a1cdddec0ae2580c8))


### Bug Fixes

* **blonkConsultants2016:** run model if value is `0` ([204867b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/204867b99db91ae9f0c8c928a715d5eecb7ab628))
* **blonkConsultants2016:** use factor value as `0` when set ([c55bd7d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c55bd7dc63486e66c5b5dec43a6b2465f6ecbd16))
* **co2ToAirUreaHydrolysis:** account for unspecified nitrogen urea share ([c3b9958](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c3b995810354bd8c035d62b1de8a92fc6e08d3ad))
* **cycle input aggregated:** use previous decade or generic crop for seed ([0050295](https://gitlab.com/hestia-earth/hestia-engine-models/commit/00502957c286e2d75ab79eb4f1ad1679787c28a4))
* **freshwaterWithdrawals:** account for `inputs` value ([1566a15](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1566a15721c797d421596ab7b92b7148782140a9))
* **freshwaterWithdrawals:** convert `m3` to `litre` ([a6a75e0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a6a75e05ca08efffef0a31d6a287e5e65341b4a9))
* **ipcc2006:** allow histosol to be `0` ([d403d83](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d403d83dcdfe3e5abc686bcf8413e2090a7f69bc))
* **ipcc2006:** handle completeness should return `0` value ([a962ebd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a962ebda729a418667d584cbcd662ea709c1802e))
* **nh3ToAirOrganicFertilizer:** use tanContent instead of factors ([6e2e4b2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6e2e4b2af81caa8f54e4165ff8a2e43a58257e59))
* **otherBackgroundDatabase:** do not group inputs together ([aedbfee](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aedbfee7aec53dc944ac8ee38b8105eb515a2511))
* **pooreNemecek2018:** return `0` for aquaculture management if site not water ([f5bf4b8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f5bf4b80861b4d88dde3526d2339f1405f76b3de))
* **pToSurfaceWaterSoilFlux:** account for all organic fertilizers ([f68b91e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f68b91ecdc484602daa5661283eea656f436b17f))
* **utils:** find tillage by termType ([c9c8e3e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c9c8e3ebfe9bbba173758e9af75a0bad7e43f6b1))

## [0.17.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.16.0...v0.17.0) (2021-09-09)


### ⚠ BREAKING CHANGES

* **co2ToAirBiomassAndSoilCarbonStockChange:** `co2ToAirBiomassAndSoilCarbonStockChange` has been renamed to
`co2ToAirSoilCarbonStockChange`
* **noxToAirAllOrigins:** `noxToAirAllOrigins` has been renamed to `noxToAirSoilFlux`
* **n2OToAirAllOrigins:** `n2OToAirAllOrigins` has been renamed to `n2OToAirSoilFlux`
* **nErosionAllOrigins:** `nErosionAllOrigins` has been renamed to `nErosionSoilFlux`
* **pErosionAllOrigins:** `pErosionAllOrigins` has been renamed to `pErosionSoilFlux`
* **pToSurfacewaterAllOrigins:** `pToSurfacewaterAllOrigins` has been renamed to `pToSurfaceWaterSoilFlux`
* **pToGroundwaterAllOrigins:** `pToGroundwaterAllOrigins` has been renamed to `pToGroundwaterSoilFlux`
* **pToDrainageWaterAllOrigins:** `pToDrainageWaterAllOrigins` has been renamed to `pToDrainageWaterSoilFlux`
* **no3ToGroundwaterAllOrigins:** `no3ToGroundwaterAllOrigins` has been renamed to `no3ToGroundwaterSoilFlux`

### Features

* **impact assessment emissions:** ignore `deleted` emissions ([029fb51](https://gitlab.com/hestia-earth/hestia-engine-models/commit/029fb511742c8dfaf2792415d23b286dd1770f8c))


* **co2ToAirBiomassAndSoilCarbonStockChange:** rename to `co2ToAirSoilCarbonStockChange` ([5bfbf52](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5bfbf52aaafefbab33866a7c1371d82a4fb5f3df))
* **n2OToAirAllOrigins:** rename to `n2OToAirSoilFlux` ([b7b6608](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b7b660870cd6fdfb1c55c7c123d11b835f5ffaa1))
* **nErosionAllOrigins:** rename to `nErosionSoilFlux` ([dc59649](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dc59649a93b5f65215e3b118c70a7790866de4e4))
* **no3ToGroundwaterAllOrigins:** rename to `no3ToGroundwaterSoilFlux` ([bca04c9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bca04c9836e8944d57e839fc4553190ead0f1762))
* **noxToAirAllOrigins:** rename to `noxToAirSoilFlux` ([00dfb71](https://gitlab.com/hestia-earth/hestia-engine-models/commit/00dfb71fa98459058c18f5aca07ff295ec1f09b0))
* **pErosionAllOrigins:** rename to `pErosionSoilFlux` ([97cad48](https://gitlab.com/hestia-earth/hestia-engine-models/commit/97cad483377876adb54dd946ca0e2f09b03d2a55))
* **pToDrainageWaterAllOrigins:** rename to `pToDrainageWaterSoilFlux` ([a1a9273](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a1a927383d11d622ea62b7225599b641176a4ca7))
* **pToGroundwaterAllOrigins:** rename to `pToGroundwaterSoilFlux` ([9507fc0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9507fc0e1c6765c8e5cd9da8234881f5fd735054))
* **pToSurfacewaterAllOrigins:** rename to `pToSurfaceWaterSoilFlux` ([dc758e8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dc758e81243a61e07f23ebadc70f0b945f8c7f0f))

## [0.16.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.15.2...v0.16.0) (2021-09-08)


### ⚠ BREAKING CHANGES

* **ipcc2019:** `noxToAirExcreta` / `ipcc2019` has been moved to `dammgen2009`

### Bug Fixes

* **cycle input aggregated:** fix `endDate` not matching ([c8b672c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c8b672ce30044e1b91ac92fc6d1f130693a82599)), closes [#160](https://gitlab.com/hestia-earth/hestia-engine-models/issues/160)
* divide inputs impacts by product yield ([c85a147](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c85a1475969994749a34cda4b9565f8f2a170578)), closes [#163](https://gitlab.com/hestia-earth/hestia-engine-models/issues/163)


* **ipcc2019:** move `noxToAirExcreta` to `dammgen2009` model ([5bed25e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5bed25e74f8762159f9366a538cc306578de9930))

### [0.15.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.15.1...v0.15.2) (2021-09-06)


### Bug Fixes

* **heavyWinterPrecipitation:** set `0` when no precipitation ([ed0c792](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ed0c79286fd927280ce77f5af5a76a91ffcba511))
* **nh3ToAirExcreta:** handle no primary product ([3046a2b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3046a2bd4c8feabf921f5a435b968571872cd70f))

### [0.15.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.15.0...v0.15.1) (2021-09-06)


### Features

* **emeaEea2019:** add `nh3ToAirExcreta` model ([3372f51](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3372f51ab79984a203543df0dd9a324e476ab028))


### Bug Fixes

* **spatial:** use `mean` instead of `first` when necessary ([1faaa4d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1faaa4de6ee4113167b8bdbe4cd683612fdeaa2e))

## [0.15.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.14.3...v0.15.0) (2021-08-30)


### ⚠ BREAKING CHANGES

* **ipcc2019:** `no3ToGroundwaterExcreta` / `ipcc2019` has been moved to `epa2014`

### Features

* **cycle input aggregated:** search by country if provided ([c18fb63](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c18fb6322cd6374a2702f20ab91b3dd855a2d111))
* **pooreNemecek2018:** add `flowingWater` model ([60aa6de](https://gitlab.com/hestia-earth/hestia-engine-models/commit/60aa6de185d852c0349964174619f9aae41968c7))
* **pooreNemecek2018:** add `waterDepth` model ([85bbef1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/85bbef1a2d11d6209c1a5ad0e79e78ccd32fa8e2))
* **spatial:** add `waterDepth` model ([e6bf292](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e6bf292112d3f8f15e8caa13c0ff90c44c7b6095))
* **spatial:** fix log errors from API ([79a9553](https://gitlab.com/hestia-earth/hestia-engine-models/commit/79a95530c1e35068a7aea2dd9de311732f183658))


### Bug Fixes

* **utils:** handle region not starting by `GADM` ([260bfba](https://gitlab.com/hestia-earth/hestia-engine-models/commit/260bfba782abcc5d83c49fa42eaa9200b6c077ac))


* **ipcc2019:** move `no3ToGroundwaterExcreta` model to `epa2014` ([7b55b30](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7b55b3073b2d3bb69feea7ef909296954fd82d75))

### [0.14.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.14.2...v0.14.3) (2021-08-27)


### Features

* **chaudharyEtAl2015:** account for `input` values as well ([885cbd9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/885cbd9dd6e85ddf44eb14d93c29c1257819cc77))
* **chaudharyEtAl2015:** use region level 1 factors ([d5c7b0a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d5c7b0ad2b336c43fb5bd95769a181a131a424e9))
* **ipcc2019:** add `n2OToAirExcretaDirect` model ([4dc5e83](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4dc5e837f4d151d978db80df8e153ad027bbc0e0))
* **ipcc2019:** add `no3ToGroundwaterExcreta` model ([0574711](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0574711689bcfe31977ec991bc8fdd95f6b4ae93))
* **ipcc2019:** add `noxToAirExcreta` model ([b9b68aa](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b9b68aa88f69b0dbb1b7ff7450f19580ef9ba982))
* **landOccupation:** account for `input` values as well ([71927cb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/71927cb911c890d2f63131ab79cc4e019ae70172))
* **scarcityWeightedWaterUse:** account for `input` values as well ([9545e06](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9545e06effe287b1e779d4491201b3f35266b705))


### Bug Fixes

* **cycle input aggregated:** only look for global values ([ada464f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ada464fb40408435acdf6ddbd566aef417790e80))

### [0.14.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.14.1...v0.14.2) (2021-08-25)


### Bug Fixes

* **ch4ToAirExcreta:** handle no primary product ([11bdff1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/11bdff1e2f58d53efd8b7b2752b7a34dbb44f5ed))
* **otherBackgroundDatabase:** load `recalculated` version when available ([f4f4aca](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f4f4acae4fdfc20ffbc98f8d908dde12081f95c2))

### [0.14.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.14.0...v0.14.1) (2021-08-25)


### Bug Fixes

* **otherBackgroundDatabase:** skip missing `impactAssessment` on Input ([d97a9a8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d97a9a8f66e9cfdbcc93f00afd08e74ac795882d))

## [0.14.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.13.0...v0.14.0) (2021-08-25)


### ⚠ BREAKING CHANGES

* **faostat2018:** `readToCookWeightPerHead` has been renamed to `readyToCookWeightPerHead`

### Features

* **ipcc2019:** add `ch4ToAirExcreta` model ([8ec16ab](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8ec16ab94c4211d53db3eb519c784359756c6344))
* **scarcityWeightedWaterUse:** run model if `country` provided ([e055e90](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e055e908447dc7c5fa017c5c002dc4435e2a6f3d))


### Bug Fixes

* **data:** fix load impacts from download dir override env var ([cc724c5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cc724c56c34ff03ef3a527c260cf35811c3277a2))


* **faostat2018:** fix typo `readyToCookWeightPerHead` ([0a8abff](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0a8abff80c5a5f900d83befbc0d1fe4323500140))

## [0.13.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.12.3...v0.13.0) (2021-08-14)


### ⚠ BREAKING CHANGES

* min schema version is `6.5.0`
* **ipcc2006:** `residue` and `aboveGroundCropResidue` have been moved to `koble2014` model

### Features

* **data download:** enable override dest dir with `DOWNLOAD_DIR` env variable ([1c602ec](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c602ec97e3033c835774a7b3960a3fbeae9c354))
* require min schema version `6.5.0` ([153ebc5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/153ebc5f7e13f8f35c26d0fdb9d48dc04ee45971))


* **ipcc2006:** move `residue` to `koble2014` ([58771e9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/58771e9133d0ddd7848718938a9c35bd5f33a5fa))

### [0.12.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.12.2...v0.12.3) (2021-08-10)


### Bug Fixes

* **ch4ToAirAquaculturePonds:** restrict to `excretionIntoWaterBody` practice only ([a546fd8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a546fd8da5b9f7b31ae551d0df7f6dc65195b8b9))

### [0.12.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.12.1...v0.12.2) (2021-08-09)


### Bug Fixes

* **nh3ToAirAquaculturePonds:** handle freshwater sites ([3709cc8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3709cc825c69b752c0b190148119249edcb9dbd6))

### [0.12.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.12.0...v0.12.1) (2021-08-09)


### Bug Fixes

* **utils:** handle find term property no term `[@id](https://gitlab.com/id)` ([fedf265](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fedf265f401e0a647e3bbeff73eb3677728e31cd))

## [0.12.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.5...v0.12.0) (2021-08-09)


### ⚠ BREAKING CHANGES

* **excreta:** `excreta` has been renamed to `excretaKgN`
* **freshwaterWithdrawals:** `freshwaterWithdrawals` is now under the `impact_assessment` model

### Features

* **excretaKgVs:** run model with `animalFeed` incomplete and excreta `as N` ([44c7495](https://gitlab.com/hestia-earth/hestia-engine-models/commit/44c7495f75b59261fd732e7b50f307afc9cc8733))
* **pooreNemecek2018:** add `excretaKgVs` model ([5678219](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5678219848b07a11555d96b878a9ccf29ab5da73))


### Bug Fixes

* **chaudharyEtAl2015:** skip model if no crop grouping ([4e2519c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4e2519ceb6b21414c6593a6ca8a84eaa95ace921))
* **cml2001NonBaseline:** return no indicator if no data ([a77a78a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a77a78a8cec9ef8b3ff1f910cdb455e38d722fe0))
* **organicCarbonPerKgSoil:** fix incorrect unit ([b71d6b5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b71d6b5b412981e070065d42fa46118be31fd0b4))
* **pooreNemecek2018:** handle all `excreta` terms for aquaculture models ([d2d1f81](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d2d1f813e8f7c2a64769b397cda73a2d6a853a1b))
* **rootingDepth:** handle no `dataCompleteness` ([5f2fd48](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5f2fd48bc3dc6f6f47b55f07fafaffc5c11201c4))


* **excreta:** rename to `excretaKgN` ([918e525](https://gitlab.com/hestia-earth/hestia-engine-models/commit/918e5256c4041fef92449aff219ae63538e0e68f))
* **freshwaterWithdrawals:** move to `impact_assessment` model ([5c4d822](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5c4d822590e9b4b62c297c929117804399e1a039))

### [0.11.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.4...v0.11.5) (2021-08-03)


### Bug Fixes

* **measurement:** return `0` instead of `None` when measurement not found ([8b2bda8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8b2bda8c8535620e71d675f97eb2837a0d509eff))
* **schererPfister2015:** fix no `precipitation` ([57047af](https://gitlab.com/hestia-earth/hestia-engine-models/commit/57047aff6349355532d46c1917f8b018b0f15678))

### [0.11.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.3...v0.11.4) (2021-08-03)


### Features

* **cycle:** add `liveAnimal` model ([bf326ae](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bf326ae88b3cb831826be2b1ebf73d2b98b36adc))
* **excreta:** handle `liveAquaticSpecies` with `nitrogenContent` ([92eeeaf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/92eeeaf20f80710e930af5ad37d8d33582782cfd))
* **faostat2018:** add `carcassWeightPerHead` model ([a56c965](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a56c9655326599361a98aa0e204feeb0cd898051))
* **faostat2018:** add `dressedCarcassWeightPerHead` model ([ffeab87](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ffeab877c6b2ebd7436bf431648df638c3689758))
* **faostat2018:** add `liveweightPerHead` model ([1cb526c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1cb526cf288e68374b6fcc3051b55978027b826e))
* **faostat2018:** add `readToCookWeightPerHead` model ([ade4ba9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ade4ba9925dec5f58e120869039839136247ace4))
* **pooreNemecek2018:** add `ch4ToAirAquaculturePonds` model ([aa49038](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aa490385e4d1ec4b819b189bc7fb6d16fc47b3da))


### Bug Fixes

* **utils product:** account for `liveAquaticSpecies` for animal produced ([afb80b4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/afb80b4b01945a5ffa64ad6a73d2a5edf40e74e0))

### [0.11.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.2...v0.11.3) (2021-07-26)


### Bug Fixes

* **noxToAirAllOrigins:** skip run if `ecoClimateZone` not found ([9a40869](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9a4086920eb4fcc2144ea285264682461056530d))

### [0.11.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.1...v0.11.2) (2021-07-26)


### Features

* **ipcc2006:** add debug `nh3` and `nox` values ([ac90a23](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ac90a23c5b534012254983c28891f7958aa9883a))


### Bug Fixes

* **aboveGroundCropResidueTotal:** do not run when no product `value` ([386b232](https://gitlab.com/hestia-earth/hestia-engine-models/commit/386b23264f048700312f5113f3b4919410c23909))
* **belowGroundCropResidue:** do not run when no product `value` ([988d6ec](https://gitlab.com/hestia-earth/hestia-engine-models/commit/988d6ecb14b4a8fef8ed7bc262401caa69fdbe24))
* **impact assessment download:** force re-download on purge ([3ffabfa](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3ffabfab5294a802546f8fe18132165d6140e57d))
* **n2OToAirCropResidueDecompositionIndirect:** account for both `nox and nh3` ([9c53c9c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9c53c9c0ee83dbf0f4210d4bae97b7214fbd07eb))

### [0.11.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.11.0...v0.11.1) (2021-07-23)


### Bug Fixes

* **excreta:** handle no primary product ([bd710e6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bd710e69cf5de180127618da367b9b1ad6878b40))

## [0.11.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.10.1...v0.11.0) (2021-07-23)


### ⚠ BREAKING CHANGES

* use schema min `6.0.0`

### Features

* **pooreNemecek2018:** add `excreta` model ([26d20eb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/26d20eb30432b0a31bb5da4c6f93cd68340334b8))


* handle `practice.value` as an array ([a926040](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a926040a475b309f0e63422c9b8b8b4562123f3c))

### [0.10.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.10.0...v0.10.1) (2021-07-22)


### Features

* **requirements:** update schema to `5.3.0` ([1fa1d66](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1fa1d6619964a3727c550d2765ce55bc09def426))

## [0.10.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.9.1...v0.10.0) (2021-07-22)


### ⚠ BREAKING CHANGES

* update to this version to use latest glossary terms

### Bug Fixes

* replace `inorganicNitrogenFertilizerUnspecifiedAsN` with new term ([09c584d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/09c584dcaa974ac3b54d702f6c0b6ccdfde08075))

### [0.9.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.9.0...v0.9.1) (2021-07-21)

## [0.9.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.8...v0.9.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* replace `soilOrganicCarbonContent` models by `organicCarbonPerKgSoil`
* replace `soilPhosphorusContent` models by `phosphorusPerKgSoil`
* replace `soilTotalNitrogenContent` models by `totalNitrogenPerKgSoil`
* **requirements:** minimum version of schema is now `5.0.0`

### Features

* **ipcc2019:** add `ch4ToAirFloodedRice` model ([38021db](https://gitlab.com/hestia-earth/hestia-engine-models/commit/38021dbc2ce7e4cafe319e76daf1bf86d64c8739))
* **ipcc2019:** add `croppingDuration` model ([4ff644b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4ff644b29bfb74a530423cd737c512c1e3205708))


### Bug Fixes

* **pooreNemecek2018:** require `excretaKgN` instead of `excretaAsN` ([c949a0c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c949a0c4665867d68c425ab8303b6a56e00b4151))
* **transformation post checks:** do not set `products` if model returns nothing ([d83d649](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d83d6495a0c6960569464e8dae89392d9122b439))


* rename `soilOrganicCarbonContent` by `organicCarbonPerKgSoil` ([8301b2d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8301b2d45cd4cdae9c95a2947d714a6c3990c053))
* rename `soilPhosphorusContent` by `phosphorusPerKgSoil` ([0a49303](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0a49303236d08fb0da32c06255afd3e695648254))
* rename `soilTotalNitrogenContent` by `totalNitrogenPerKgSoil` ([1299259](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1299259c1d6cad39082e2a758df0c60a9fedaf21))
* **requirements:** use schema `5.0.0` ([8434982](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8434982e27d6225b1383cd929a7ea26d78ff8522))

### [0.8.8](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.7...v0.8.8) (2021-07-14)


### Bug Fixes

* **n2OToAirAquaculturePondsDirect:** convert value to `N` ([5c26922](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5c2692265a23d5a291901b3d2577eccf0128c65e))
* **nh3ToAirAquaculturePonds:** convert value to `N` ([4c75091](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4c75091e32809a9141ec3d9e9b4dc4de65a2d598))
* **noxToAirAquaculturePonds:** convert value to `N` ([ea09ab9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ea09ab9f3a07f1f0ab0bc1287183f9bf5a4cd6be))

### [0.8.7](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.6...v0.8.7) (2021-07-13)


### Features

* **cycle aggregated:** add impact assessment of seed ([558a075](https://gitlab.com/hestia-earth/hestia-engine-models/commit/558a075c27a2ffd5b7aa3e4edbeae1c2cca95ad2))
* **model links:** add new models ([f937132](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f937132a1e3fd7ce6c30cf7de82c70c222eec582))
* **pooreNemecek2018:** add `n2OToAirAquaculturePondsDirect` model ([9c600ee](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9c600ee2c950b88365657993ac07b00113b7e30e))
* **pooreNemecek2018:** add `n2ToAirAquaculturePonds` model ([ba02a1c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ba02a1c53d01aded85ac90a76e3c36fbb6c2b7da))
* **pooreNemecek2018:** add `netPrimaryProduction` model ([81ab21e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/81ab21e10bdc49604a0cb4294e19bb18ef811c36))
* **pooreNemecek2018:** add `nh3ToAirAquaculturePonds` model ([77457e9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/77457e932945004c8a072f2f1537ff19ce922527))
* **pooreNemecek2018:** add `noxToAirAquaculturePonds` model ([5e1b67e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5e1b67e053250292dc6d69312bf5551d12f4f5f2))
* **seed:** handle multiple crop products ([fd2ea33](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fd2ea335ceb2b25d99ecb467d8569ca63044c618))


### Bug Fixes

* **ecoClimateZone:** round value to integer ([aff15e1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aff15e115d9e693b5678a4232ddce4002ffee317))
* handle no `aboveGroundCropResidueTotal` product ([790b4bf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/790b4bf7c85f3037a7e1480504f5ad685ea0bd25))
* **impact assessment emission:** handle no product found ([825129e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/825129e1238a1041f10791fb172b7e5f2176f4c8))
* **impact assessment emissions:** only create if product has `value` ([a6e8c32](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a6e8c32c7692c8e5d46645149ec1920fe24b497a))
* **landOccupation:** only run if product has `value` ([4616617](https://gitlab.com/hestia-earth/hestia-engine-models/commit/46166171e3074a6e017cada0ee51ec7fd59898ef))

### [0.8.6](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.5...v0.8.6) (2021-07-07)


### Features

* **transformation:** add `post_checks` model ([d80803c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d80803c8e034d6dccbe5a4307eebe8bf2fd25011))

### [0.8.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.4...v0.8.5) (2021-07-06)


### Bug Fixes

* **otherBackgroundDatabase:** fallback for impact not recalculated ([5a4fc9e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5a4fc9ed2461711a5466476d627c3eecd43a00a4))

### [0.8.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.3...v0.8.4) (2021-07-06)


### Bug Fixes

* **ch4ToAirEntericFermentation:** handle no primary product ([46d4e66](https://gitlab.com/hestia-earth/hestia-engine-models/commit/46d4e666d392b43708801314307eb9b58d2a98bc))
* **ipcc2006:** handle no primary product when calculating land occupation ([4f74d21](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4f74d21204df616fa80954b00efcc444562b8e47))
* **n2OToAirCropResidueDecompositionDirect:** fix run when it should not ([9fd5c5f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9fd5c5ff6e63b490e997464a77f712792e0daebe))
* **otherBackgroundDatabase:** load recalculated impact only if added from model ([3bf7148](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3bf714847b4820982b1b14597dc3aafcdf11a377))
* **otherBackgroundDatabase:** return emissions with `inputs` only ([81e1c62](https://gitlab.com/hestia-earth/hestia-engine-models/commit/81e1c621eb0397866e1ec2b446e1acf27598a659))

### [0.8.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.2...v0.8.3) (2021-07-05)


### Bug Fixes

* **ipcc2006:** sum all `no3` emissions to calculate value ([ab32605](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ab326055fca0f85320bdc1647a6a7418c27fce4c))
* rename `grossEnergyContent` to `grossEnergyContent` ([bd20526](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bd205260ef18312a5f76f6ea2717ae306dd2b039))

### [0.8.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.1...v0.8.2) (2021-07-02)


### Bug Fixes

* **ch4ToAirEntericFermentation:** handle no feed in cycle ([03af84d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/03af84d04f2029199cc07d24c6ab57bdb0d5f071))

### [0.8.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.8.0...v0.8.1) (2021-07-02)


### Bug Fixes

* **ch4ToAirEntericFermentation:** handle no `crop` input ([45c0da0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/45c0da088f178908bb4b6eb615eebede46368ede))
* **cycle input aggregated:** use exact match for product name ([d6d81d4](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d6d81d44f98d62a0264410dc8f4808837b9763db))

## [0.8.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.7.2...v0.8.0) (2021-07-02)


### ⚠ BREAKING CHANGES

* **ecoinventV3:** use `input.ecoinventV3` as model key for `cycle`
* **ecoinventV3:** to calculate `ecoinventV3` emission, need to also run `otherBackgroundDatabase`

### Features

* **ch4ToAirEntericFermentation:** handle `ionophores` ([b402694](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b40269461548473dfb4ac3aef5919d986936e344))
* **ch4ToAirEntericFermentation:** handle digestibility and NDF ([21f3788](https://gitlab.com/hestia-earth/hestia-engine-models/commit/21f37888f70716bb454969bfe92d170e3929e7f9))
* **cycle:** add `siteDuration` model ([21b4319](https://gitlab.com/hestia-earth/hestia-engine-models/commit/21b431907421a17a985e4f5d660fd8c7f22a768b))
* **cycle input:** add `aggregated` model ([29aae43](https://gitlab.com/hestia-earth/hestia-engine-models/commit/29aae438d26dd05b14596674e23a256b74408d86))
* **otherBackgroundDatabase:** group emissions by inputs production ([f0860a5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f0860a51c168ea66cc62166b32a2db5b7526d1a3))
* **requirements:** use schema `4.2.0` ([c9a44fe](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c9a44fe932b2b427fced3ef7bd810ef427ba062f))
* **requirements:** use schema `4.4.0` ([1ce5e60](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1ce5e600ab305731d9cc2cd3312872cbf357cbe3))


### Bug Fixes

* **ch4ToAirEntericFermentation:** divide result by `55.65` ([a26e7e8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a26e7e8bca66d75f68995080926fedca8706904f))
* **ecoinventV3:** combine with `otherBackgroundDatabase` to create `Emission` ([ec1f6d0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ec1f6d0273c8f03a806e7e4bab5f0650a4017fd1))
* **utils cycle:** handle no product value for `landOccupation` ([490f43d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/490f43dc80231627a91ae4fa5cdb428a5f79f0ae))


* **ecoinventV3:** move to `cycle.input` model ([9c7e102](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9c7e1021c1c8ea9ca713d6f2650ddef7082d8e00))

### [0.7.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.7.1...v0.7.2) (2021-06-12)


### Bug Fixes

* update lookup table column for `ecoClimateZone` ([298fc90](https://gitlab.com/hestia-earth/hestia-engine-models/commit/298fc907eec63eddbe3d335d2fe7659b00728e73))
* **ch4ToAirEntericFermentation:** use `liveAnimal` instead of `animalProduct` ([a47179f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a47179fb28b7df9bec9a8e5d3b6de42629dd9cf5))
* **histosol:** update collection to return a percentage ([3c27dcb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3c27dcb1d70b968b4bfe904fc4f4f02881d42b4d))
* **n2OToAirOrganicSoilCultivationDirect:** add missing convertion factor ([90395b2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/90395b2145e66322774eecfb7dfc66b2a927ac3f))

### [0.7.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.7.0...v0.7.1) (2021-06-10)


### Features

* **cycle:** add `feedConversionRatio` model ([0328385](https://gitlab.com/hestia-earth/hestia-engine-models/commit/03283854daa30999813ee7b5cd8ab4e78e744624))

## [0.7.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.6.1...v0.7.0) (2021-06-08)


### ⚠ BREAKING CHANGES

* **nh3ToAirInorganicFertilizer:** `europeanEnvironmentAgency2016` model no longer exists

### Features

* only run models when `siteType` is `cropland` ([73fc6b2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/73fc6b2d547ff5536c5042441a4f065a1c828760))
* **chaudharyEtAl2015:** add `biodiversityLossLandOccupation` model ([4d29016](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4d29016a5671bcbfceb8a9bface714cbb7e32316))
* **chaudharyEtAl2015:** add `biodiversityLossLandTransformation` model ([560e691](https://gitlab.com/hestia-earth/hestia-engine-models/commit/560e69192841d89dbc69da274c2adc3492b23d4d))
* **chaudharyEtAl2015:** add `biodiversityLossTotalLandUseEffects` model ([3907889](https://gitlab.com/hestia-earth/hestia-engine-models/commit/39078891a3bc8e7760b48d4c2c2951776c2b28b0))
* **pooreNemecek2018:** add `soilTotalNitrogenContent` model ([45707a9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/45707a90444e2b16cceb5b159d6a85e2b478703d))
* **spatial:** only run models when `siteType` is `cropland` or `permanent pasture` ([8e93d47](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8e93d4713fb7b7d15e9968bd15b3b829b3c390d2))


### Bug Fixes

* **co2ToAirOrganicSoilCultivation:** use land occupation instead of land transformation ([e8c4cf9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e8c4cf9eca0844a9a809558a832ffcfa91e68666))
* **n2OToAirOrganicSoilCultivationDirect:** use land occupation instead of land transformation ([f212b80](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f212b806f08528140ac5639c1f50e4647addb599))


* **nh3ToAirInorganicFertilizer:** merge models in `emeaEea2019` ([a0a55b6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a0a55b6a4ccdfd9e872eb50d9577a07c5dc3a9fb))

### [0.6.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.6.0...v0.6.1) (2021-06-07)


### Features

* add `statsDefinition` on `Property` ([9e6afaf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9e6afaf966b395c76aa20079eb1f355512776b60))
* add `statsDefinition` to all `Emission` and `Indicator` ([388a682](https://gitlab.com/hestia-earth/hestia-engine-models/commit/388a682397470c8cb9082c3bcbdc07b28c9eee2b))
* **ch4ToAirEntericFermentation:** add `sd` field ([e7b50d9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e7b50d91a86f7f6277bb2da14bd705fd538cf682))
* **dataCompleteness:** add `soilAmendments` to model ([0e59c82](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0e59c82980c2e4e2e4d01bac58fec8cb7ff830ef))
* **ipcc2006:** add `statsDefinition` for crop residue models ([bcd2c6f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/bcd2c6f439a8ed4b6e5447bed4f4b0b9ba34fa3d))
* **model links:** add `ch4ToAirEntericFermentation` model ([3837fe2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3837fe2367a3fd5bc89d9085b02311b11395c2bc))
* **requirements:** use schema `3.7.0` ([a48030b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a48030bfba04096609de19618581b1fe5a0d2367))
* **requirements:** use schema `4.0.0` and utils `0.6.0` ([e1bbe76](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e1bbe76651ae907b316e98d562525261f9c12af3))
* **spatial:** add `croppingIntensity` model ([58dc15a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/58dc15a8039c046e8b6818482732917746cd60e0))


### Bug Fixes

* **aboveGroundCropResidueTotal:** handle removed practice with value of `0` ([f563ebf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f563ebf37e6b9030ba1112b7b829f09e71daf634))
* **ch4ToAirEntericFermentation:** add missing division factors ([b46bf0a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b46bf0a85b81a522dd130890db24220bc7dc4e04))
* **croppingIntensity:** handle division by zero ([223f2c8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/223f2c8707bdc143a31094b940a9943403946b8a))
* **ipcc2006:** get excreta instead of animal product ([96255be](https://gitlab.com/hestia-earth/hestia-engine-models/commit/96255be522c234d9dea07e2907f16933a92443c8))
* **landOccupation:** handle no `economicValueShare` ([1a6e874](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1a6e874e135763d81c5eb47b0491555cab90edb4))
* **landOccupation:** handle no `product.value` ([85ed8f7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/85ed8f7a540bb5aa5c0e0dcee2698f060e3e2011))
* **model links:** add missing `croppingIntensity` ([7cbd07d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7cbd07db078aa6a1e09b52d8e7095f4a2868fad3))
* **pooreNemecek2018:** get excreta N_total instead of animalProduct ([96dfe64](https://gitlab.com/hestia-earth/hestia-engine-models/commit/96dfe64f7cd801bb03383690e74620031aea67ed))
* **pToSurfacewaterAllOrigins:** skip excreta input from model ([2e3522a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2e3522a18d6a669854aedee5b1cdd9d80a0d0f19))
* **stehfestBouwman2006:** get excreta N_total instead of animal product ([75637d7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/75637d747ca34be5a44c220ec87173f6ce6caf2f))

## [0.6.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.5.2...v0.6.0) (2021-05-22)


### ⚠ BREAKING CHANGES

* **n2OToAirFuelCombustion:** use `n2OToAirFuelCombustionDirect` instead of `n2OToAirFuelCombustion`

### Features

* **ipcc2019:** add `ch4ToAirEntericFermentation` model ([9f2b718](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9f2b718bb16019e284ba4d8c5480db70e1625a88))


### Bug Fixes

* **model links:** add missing model ([aa8882b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aa8882bd6da72277a4e4ac9be5ef4711d04df39d))


* **n2OToAirFuelCombustion:** rename to `n2OToAirFuelCombustionDirect` ([0f37e8d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0f37e8d9e42108dc11d64e7bc94e0776af93d684))

### [0.5.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.5.1...v0.5.2) (2021-05-21)


### Features

* **aware:** add `freshwaterWithdrawals` model ([9e3a056](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9e3a05611b7a546af9a711184562bb93ca62007d))
* **pooreNemecek2018:** add `landOccupation` model ([66fb1b0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/66fb1b0d9963917af0c379fdce69a17085615007))


### Bug Fixes

* **cycle product price:** skip run no country ([df55223](https://gitlab.com/hestia-earth/hestia-engine-models/commit/df552238ebc0e1335a5e5d9f1606c194940d4e6f))
* **fallowCorrection:** handle 0 value ([fcfa251](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fcfa25156b9d545bb96c2cfb1a5a0419ce2e9547))
* **impact organic:** handle practices without term ([d4876fb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d4876fb0d7200cabf34164ff508cba8f8f543cb9))
* **scarcityWeightedWaterUse:** use `freshwaterWithdrawals` to calculate value ([622df3a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/622df3abae0f4f36c2a29edb6d843d7d1fe82135))

### [0.5.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.5.0...v0.5.1) (2021-05-19)


### Bug Fixes

* **utils term:** increase search limit to 100 ([571ae2d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/571ae2dc3fd8a2816571cea227c283b1e6b62d22))

## [0.5.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.4.0...v0.5.0) (2021-05-19)


### ⚠ BREAKING CHANGES

* **gwp100ExcludingFeedbacks:** use `ipcc2013ExcludingFeedbacks.run('gwp100')`
* **gwp100IncludingFeedbacks:** use `ipcc2013IncludingFeedbacks.run('gwp100')`

### Features

* **model links:** add missing impactassessment models ([1c35f49](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c35f491e072fe87794e282fdad1f4f8cf8f411a))


### Bug Fixes

* **cycle:** do not set `site` to `None` if no `[@id](https://gitlab.com/id)` ([f5d18ac](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f5d18acf2c2e779fdeb99d772c0f885c37f633e6))
* **impact assessment:** do not set `cycle` or `site` to `None` if no `[@id](https://gitlab.com/id)` ([c71cd36](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c71cd36579b9a51205b0109a52afa26a0e2a2d71))
* **log:** set LEVEL from env for file logging ([b82b010](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b82b0104e4e33c3073d96fc38cb4a07b1912462e))
* **residueBurnt:** handle no percent found for country ([993f482](https://gitlab.com/hestia-earth/hestia-engine-models/commit/993f482c0e5e94f1be1cf28e6f19815167fa7d63))


* **gwp100ExcludingFeedbacks:** move to `ipcc2013ExcludingFeedbacks` as `gwp100` ([86f5238](https://gitlab.com/hestia-earth/hestia-engine-models/commit/86f52381fefdac1e1a890ce5a81829498e0b6609))
* **gwp100IncludingFeedbacks:** move to `ipcc2013IncludingFeedbacks` as `gwp100` ([a6ef263](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a6ef2631de0a81a726e9ad948d1063dfc34ca20f))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.3.0...v0.4.0) (2021-05-18)


### ⚠ BREAKING CHANGES

* **co2ToAirSoilCarbonStockChange:** use `co2ToAirBiomassAndSoilCarbonStockChange` instead of
`co2ToAirSoilCarbonStockChange`

### Features

* **blonkConsultants2016:** add `ch4ToAirNaturalVegetationBurning` model ([a5ecf4d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a5ecf4da233d99ceb8d738d78c02a8db90d0c6cf))
* **blonkConsultants2016:** add `n2OToAirNaturalVegetationBurningDirect` model ([41a7701](https://gitlab.com/hestia-earth/hestia-engine-models/commit/41a7701a90e13e3e5272a5212b8768db012c6a4d))


### Bug Fixes

* **faostat2018:** remove `co2ToAirOrganicSoilBurning` model ([e39c0a0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e39c0a096dba5a56cb9448d796556fca839bf353))


* **co2ToAirSoilCarbonStockChange:** rename to `co2ToAirBiomassAndSoilCarbonStockChange` ([1c98101](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1c98101a397e3d7ebf9eac7c5d0b3a575a05ad31))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.5...v0.3.0) (2021-05-18)


### ⚠ BREAKING CHANGES

* **longFallowPeriod:** use `fallowCorrection` as `Measurement` instead of `longFallowPeriod`

### Features

* extract model link from sub-models ([7f00f67](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7f00f670dd2dcef57f0bc48a760d7b4124d5d48a))


### Bug Fixes

* **co2ToAirOrganicSoilBurning:** use `fallowCorrection` instead of `landOccupation` ([841319b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/841319bfe3644c40ca1423484f211424a84f9394))
* **co2ToAirSoilCarbonStockChange:** use `fallowCorrection` instead of `landOccupation` ([b3724c9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b3724c9d1bf5eb7533d0fe35ab19e142f56209ae))
* **impact assessment emissions:** handle no product `value` ([aab5aa2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aab5aa283d422166e7368366ec839b8c537b963b))
* **residue:** handle skip model if no primary product ([858076c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/858076c66a01e2bc89a5e3fbc404c1b68e13de4b))
* **utils:** handle no `method` ([0a0ff9f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0a0ff9f8824bfabb7e722c182b59fddd70573198))


* **longFallowPeriod:** rename to `fallowCorrection` model ([ea923ca](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ea923ca0bd417ff0b92a700b3d202af59e72f8e6))

### [0.2.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.4...v0.2.5) (2021-05-13)


### Features

* **ipcc2006:** add `co2ToAirOrganicSoilCultivation` model ([7771901](https://gitlab.com/hestia-earth/hestia-engine-models/commit/777190104dd341f5b66a5109eb3bb5f1a30c7c67))
* **ipcc2006:** add `n2OToAirOrganicSoilCultivationDirect` model ([b9d9dae](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b9d9dae1a82f7e10c3b22f52352cba602aaf3c7d))
* **requirements:** use schema `3.4.0` ([f54eb59](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f54eb59add754d76d2162c9fbd730bd5dff5a3b1))

### [0.2.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.3...v0.2.4) (2021-05-11)


### Features

* **aware:** add `scarcityWeightedWaterUse` model ([5b7c7c2](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5b7c7c2d280cda9271ca07bc1cab7c8cba23bf5b))
* **impact_assessment:** add `site` pre/post checks ([8eef77b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8eef77bda06911355f217a7f1da19e6544dbc7a4))


### Bug Fixes

* **spatial raingallAnnual:** use `reducer_regions` = `mean` ([b90e7a3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b90e7a35c2a0b121c4b47572580e601abeb5715e))

### [0.2.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.2...v0.2.3) (2021-05-11)


### Features

* **europeanEnvironmentAgency2016:** add `nh3ToAirInorganicFertilizer` model ([aa94c77](https://gitlab.com/hestia-earth/hestia-engine-models/commit/aa94c7719cd95bf96cc8754eb0766a85c3a9dd5b))


### Bug Fixes

* **emeaEea2019:** fix `nh3ToAirInorganicFertilizer` model ([fe84bf7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fe84bf7d1a15e5ba1624b357a9a972f1c1cff53b))

### [0.2.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.1...v0.2.2) (2021-05-10)


### Features

* log model used per term ([084284e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/084284ea9668e5f899ac94fd1a1cc15fa44be0e2))
* **schererPfister2015:** add `pErosionAllOrigins` model ([2adb951](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2adb951cbfe37e3b2a75330d6aa39ce0970dc412))
* **schererPfister20150:** add `nErosionAllOrigins` model ([e679deb](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e679deb735b4e6ba98944ef0f2e69b2037d736d2))


### Bug Fixes

* **utils:** fix input value without property factor ([4560cdf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4560cdfed6620aa6b7c0452d51302143a2298cf5))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.2.0...v0.2.1) (2021-05-10)


### Features

* update schema to 3.3.0 ([4f41ed3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4f41ed3abced3d241f7b8522d027a573ceea007c))
* **cycle:** add `input.value` model ([8c14b26](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8c14b26756055f932c35507d410f3a1ccbf74fe4))
* **cycle:** add `product.value` model ([f98b781](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f98b7812493711d3889678205bf45f7a8ed2c23b))
* **cycle product price:** add debug info on lookup content ([01f74e3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/01f74e3e9e6fce85d269084487f463111f9aa1eb))
* **spatial:** add `longFallowPeriod` model ([a678547](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a678547b3ae7c70cb86843c7301019c8b8d35ddd))


### Bug Fixes

* **nh3ToAirInorganicFertilizer:** set to `tier 1` instead of `tier 2` ([17d1180](https://gitlab.com/hestia-earth/hestia-engine-models/commit/17d11809452c68e430dd43439bb27b5e9ac2fc5b))
* **pooreNemecek2018:** fix leaching factor conditions ([9abf455](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9abf4553ed895d50af1daa7ea1adcee560c386e1))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.1.1...v0.2.0) (2021-05-07)


### ⚠ BREAKING CHANGES

* **ipcc2006Tier1:** use `model: ipcc2006` instead of `model: ipcc2006Tier1`

### Features

* **blonkConsultants2016:** add `co2ToAirSoilCarbonStockChange` model ([623be0b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/623be0b7d983ad2a484ec108784e10b3750fcfe3))
* **cml2001NonBaseline:** add `eutrophicationIncludingFateAverageEurope` model ([59da342](https://gitlab.com/hestia-earth/hestia-engine-models/commit/59da34295b05b81a4ffdb5e20f7518cdc24feb06))
* **cml2001NonBaseline:** add `eutrophicationPotentialExcludingFate` model ([e29801c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e29801c3b93b28e089a3adbbb8ca4775d3389993))
* **cml2001NonBaseline:** add `terrestrialAcidificationPotentialExcludingFate` model ([7ff49bc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7ff49bccd508acb214406c330bf002e4d54b1562))
* **cml2001NonBaseline:** add `terrestrialAcidificationPotentialIncludingFateAverageEurope` model ([b73ff5b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b73ff5bc54e402ee968a14916aab146312a530e2))
* **cycle product price:** update lookup table name and grouping ([b267731](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b2677318911c5df95670b31daf296d1561bc393b))
* **deRuijterEtAl2010:** add `nh3ToAirCropResidueDecomposition` model ([a4bcd64](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a4bcd64664bbc93bf5bba516448dfa67dd52582c))
* **emeaEea2019:** add `nh3ToAirInorganicFertilizer` model ([e525b53](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e525b53afd265cc5705581152e02a9243dc42c5a))
* **faostat2018:** add `co2ToAirOrganicSoilBurning` model ([9df9add](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9df9addefc249c9751ada4339ed81cb92c8f69cf))
* **impact_assessment:** add `emissions` model ([b78824a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b78824a6d24c519313b6125e385fb01ef601a797))
* **ipcc2006Tier1:** add `co2ToAirLimeHydrolysis` model ([2ae34da](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2ae34da7d3c5ad96c3ce58055c4808159b4434fb))
* **ipcc2006Tier1:** add `co2ToAirUreaHydrolysis` model ([f8cbac6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f8cbac6f807328d0168796a173bf369278345265))
* **ipcc2006Tier1:** add `n2OToAirExcretaDirect` model ([9ad4715](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9ad471576b4147f9c759fd13eb3516e136994821))
* **ipcc2006Tier1:** add `n2OToAirInorganicFertilizerDirect` model ([e5b5c20](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e5b5c20cdf61f5a6c72c8e6ea420f13ba04b8e07))
* **ipcc2006Tier1:** add `n2OToAirOrganicFertilizerDirect` model ([a85fea9](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a85fea9aa457e210449e3e130fecee30304fe5f5))
* **ipcc2013:** add `gwp100ExcludingFeedbacks` model ([9e87660](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9e87660c700f4885816f8c25d7be29a6bd6ff42b))
* **ipcc2013:** add `test_gwp100IncludingFeedbacks` model ([9dce574](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9dce574357cca870570f5d6214819397c81d17f7))
* **pooreNemecek2018:** add `no3ToGroundwaterAllOrigins` model ([5ce200e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5ce200e49c6e33e6b68dc359abe0043a6f408ae6))
* **pooreNemecek2018:** add `no3ToGroundwaterCropResidueDecomposition` model ([b9e24dc](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b9e24dc51e716b2891d50bb973a47c9aa9b175ab))
* **pooreNemecek2018:** add `no3ToGroundwaterExcreta` model ([dd87bcf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dd87bcf24334702111a7562fb53fa87e024b34b8))
* **pooreNemecek2018:** add `no3ToGroundwaterInorganicFertilizer` model ([c19d32e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c19d32ef69de05e4628e74e94383aedc3fe893c1))
* **pooreNemecek2018:** add `no3ToGroundwaterOrganicFertilizer` model ([79bd809](https://gitlab.com/hestia-earth/hestia-engine-models/commit/79bd809d94e3016646153090420b09ede97b1811))
* **pooreNemecek2018:** add `nurseryDuration` model ([b7f8639](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b7f86391d8f13a6a6fe1b555d9f6bf9dbb074f79))
* **pooreNemecek2018:** add `orchardDensity` model ([c0391f7](https://gitlab.com/hestia-earth/hestia-engine-models/commit/c0391f7758984ce5121dfde1619b015172d70b10))
* **pooreNemecek2018:** add `orchardDuration` model ([2441f95](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2441f9552cb9e418e05bdb574b111c700dd91500))
* **pooreNemecek2018:** add `saplings` model ([48d086a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/48d086aa19dffb2d81e7d03f660a54c1902ce0ba))
* **schererPfister2015:** add `pToDrainageWaterAllOrigins` model ([f490116](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f4901163ca1ed00388fb493006853e8dc89dd836))
* **schererPfister2015:** add `pToGroundwaterAllOrigins` model ([451c9af](https://gitlab.com/hestia-earth/hestia-engine-models/commit/451c9afa5713b56f5c9884e4dd94b9d161a18f2d))
* **schererPfister2015:** add `pToSurfacewaterAllOrigins` model ([99f194b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/99f194bd09bfe968573263636e71f66f020bd81d))
* **spatial:** add `heavyWinterPrecipitation` model ([9d4aaf8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9d4aaf89a39af2bc662f050dab3a06e8603989a4))
* **stehfestBouwman2006:** add `n2OToAirAllOrigins` model ([20f303b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/20f303bf0a617e8fcbbdb5c48f4457be8f0a9e21))
* **stehfestBouwman2006:** add `n2OToAirCropResidueDecompositionDirect` model ([52e1f3e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/52e1f3e8555b62d71ad859ff666e37067a41423a))
* **stehfestBouwman2006:** add `n2OToAirExcretaDirect` model ([ddf8e0c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ddf8e0c55e353bda265cfc9d3156e3e2a15ab43f))
* **stehfestBouwman2006:** add `n2OToAirInorganicFertilizerDirect` model ([07eae7e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/07eae7ef68f09957f4edb315b1249ab66b92c8da))
* **stehfestBouwman2006:** add `n2OToAirOrganicFertilizerDirect` model ([d6add1c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d6add1c8ae505ed76fee363fa1ece44a5a5cd392))
* **stehfestBouwman2006:** add `noxToAirAllOrigins` model ([ced2c41](https://gitlab.com/hestia-earth/hestia-engine-models/commit/ced2c41b9dc0155257847750c5d5476bedcf0ad4))
* **stehfestBouwman2006GisImplementation:** add `noxToAirAllOrigins` model ([f352aab](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f352aab9f59b6f8ec0331f227854b4694f7ca1c9))
* **webbEtAl2012AndSintermannEtAl2012:** add `nh3ToAirOrganicFertilizer` model ([77e77ac](https://gitlab.com/hestia-earth/hestia-engine-models/commit/77e77ac019613b6c62538e94fad719ecba3ad81e))


### Bug Fixes

* account for `kg` organic fertilizers for total Nitrogen content ([b4122c1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b4122c169058f87a03a733ef7ebd8959694f54b9))
* **co2ToAirUreaHydrolysis:** run if any single fertilizer is specified ([1d4f35c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1d4f35c0b3527a30f648dedd1bd6c7a907f05ad1))
* **eutrophicationIncludingFateAverageEurope:** rename to `eutrophicationIncludingFateAverageEurope` ([9e0dd36](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9e0dd36dd5365d2977533994a839645687460563))
* **ipcc2006Tier1:** fix `residueBurnt` use combustion factor on total-removed value ([575837a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/575837a19907abbded192ba8bd0596c754f004e3))
* **n2OToAirAllOriginsDirect:** fix should run no measurements ([052540f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/052540fb459a299919b78b8f8414ccd2fc8d47d4))
* **utils input:** account for `kg P205` in total phosphate content ([348702d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/348702dc6d67ac1a1f73d9d260b07d430c75a3b9))
* **utils input:** divide nitrogenContent as percentage ([2d71393](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2d713938f70e9629ec8503092508aeb27f9400e1))


* **ipcc2006Tier1:** rename to `ipcc2006` ([3f9977c](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3f9977c284b544e3e3b8100676e6e8cc0be2d707))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.1.0...v0.1.1) (2021-04-22)


### Bug Fixes

* fix broken import module ([8f34de8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8f34de86261fa8e68ce908ca7c30cb24cd143581))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.8...v0.1.0) (2021-04-22)


### ⚠ BREAKING CHANGES

* **ipcc2019Tier1:** use `model: ipcc2019` instead of `model: ipcc2019Tier1`

### Features

* add `methodModel` whenever possible ([9ef39af](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9ef39af471c889cece1c3a6e6649026e09d4e0ce))
* **akagiEtAl2011AndIpcc2006:** add `ch4ToAirCropResidueBurning` model ([0a9ac34](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0a9ac347a40d4c5dee0662ec6bb6562ab92ed506))
* **akagiEtAl2011AndIpcc2006:** add `n2OToAirCropResidueBurningDirect` model ([93f3962](https://gitlab.com/hestia-earth/hestia-engine-models/commit/93f3962cf9b5752370e957bc58ea2343fec2eec0))
* **akagiEtAl2011AndIpcc2006:** add `noxToAirCropResidueBurning` model ([d75d448](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d75d448d070163a00725bf79c474fca3590ba594))
* **ecoinventV3:** add background models ([b6a363a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b6a363a01eac2c4e17687319f2f71b3809766249))
* **emeaEea2019:** add `so2ToAirFuelCombustion` model ([54ef06b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/54ef06bd3335d3e652dcaab58af3026eb45283b6))
* **emeaEea2019:** add co2ToAirFuelCombustion model ([20b65a1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/20b65a1dcff59221a52fd999759d3fb801b8d7ec))
* **emeaEea2019:** add n2OToAirFuelCombustion model ([5195049](https://gitlab.com/hestia-earth/hestia-engine-models/commit/51950498b692479ea25cc81f23be9afcd4d2f4c6))
* **emeaEea2019:** add noxToAirFuelCombustion model ([a5509b8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a5509b85212e1492127064fa5ee310f345dcb076))
* **globalCropWaterModel2008:** add condition cycle function unit measure is "1 ha" ([39deadf](https://gitlab.com/hestia-earth/hestia-engine-models/commit/39deadfcc17ea83243aceb03cfdbc62ce863d4f6))
* **globalCropWaterModel2008:** add threshold of 250m3 for `rootingDepth` ([2d746a3](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2d746a357182a381fd5a61c3bdf9ed2053e60d5f))
* **ipcc2006:** calculate `aboveGroundCropResidueTotal` with default value from lookup ([a10f618](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a10f618e85bf7cc7bfdadcb72c2229e1d01699e8))
* **ipcc2006:** calculate `belowGroundCropResidue` with default value from lookup ([4983893](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4983893f34fb571cee01d47a065fa8ca89253a2a))
* **ipcc2006Tier1:** add `n2OToAirCropResidueDecompositionIndirect` model ([dfa1bbe](https://gitlab.com/hestia-earth/hestia-engine-models/commit/dfa1bbe3533372f253462a322dfd1770c7899697))
* **ipcc2006Tier1:** add `n2OToAirExcretaIndirect` model ([b4325d5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/b4325d57716ff0b63f9cee8f15016756fac1dabf))
* **ipcc2006Tier1:** add `n2OToAirInorganicFertilizerIndirect` model ([d9488d1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d9488d10bbfd0ccd820f84ba0b4ecf67e7e92a40))
* **ipcc2006Tier1:** add `n2OToAirOrganicFertilizerIndirect` model ([a38f06a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a38f06a0b909d10a818d157e15d979261d1650ae))
* **stehfestBouwman2006:** add `noxToAirCropResidueDecomposition` model ([efa8626](https://gitlab.com/hestia-earth/hestia-engine-models/commit/efa862669634cf11e215957bd612b6f1e8c43be7))
* **stehfestBouwman2006:** add `noxToAirExcreta` model ([6e28286](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6e2828653ac4ed8d15761bc0f99875a765611c4f))
* **stehfestBouwman2006:** add `noxToAirInorganicFertilizer` model ([324d2b0](https://gitlab.com/hestia-earth/hestia-engine-models/commit/324d2b060558d9bb329cc655d2fabbde27376c1a))
* **stehfestBouwman2006:** add `noxToAirOrganicFertilizer` model ([cd69b76](https://gitlab.com/hestia-earth/hestia-engine-models/commit/cd69b76211f13777f6cb8f1daf3390daefe87529))
* **stehfestBouwman2006GisImplementation:** add `noxToAirCropResidueDecomposition` model ([110bb47](https://gitlab.com/hestia-earth/hestia-engine-models/commit/110bb4784b05f8d60f2ffc7eb90a338537472606))
* **stehfestBouwman2006GisImplementation:** add `noxToAirExcreta` model ([01299ba](https://gitlab.com/hestia-earth/hestia-engine-models/commit/01299baf85de48b60fc90dd1152cfe4267b41702))
* **stehfestBouwman2006GisImplementation:** add `noxToAirInorganicFertilizer` model ([03f0dd6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/03f0dd6e147549c0b3a3b1262be2a5440494488b))
* **stehfestBouwman2006GisImplementation:** add `noxToAirOrganicFertilizer` model ([fff312f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fff312f8b17e66e3d3d718e29d30945db7fd5cec))


* **ipcc2019Tier1:** rename to `ipcc2019` ([071ed44](https://gitlab.com/hestia-earth/hestia-engine-models/commit/071ed44b478465b7aead23a7bca36889544604d2))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.7...v0.0.8) (2021-04-16)


### Bug Fixes

* skip adding `properties` if no prop found ([f28f300](https://gitlab.com/hestia-earth/hestia-engine-models/commit/f28f300760b3ef2de86872cf826277566a397902))
* **cycle pre-checks:** do not add `startDate` if not calculated ([960c5c5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/960c5c5d1f808366689262b2e0f8d9e46912d3e2))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.6...v0.0.7) (2021-04-16)


### Features

* **akagiEtAl2011AndIpcc2006:** add `nh3ToAirCropResidueBurning` model ([7a370a5](https://gitlab.com/hestia-earth/hestia-engine-models/commit/7a370a5065c5e40af61865b7f3e395f5f588ef1b))


### Bug Fixes

* **ipcc2006Tier1:** fix order terms for `residue` practices ([e7e5811](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e7e581150dc35fad28206385bfae363aca90cd93))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.5...v0.0.6) (2021-04-16)


### Bug Fixes

* return a list for blank node instead of directly the blank node ([5e5e58e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/5e5e58e28fa021990cab92f4a27750114a114df9))
* **site measurement:** only return nodes with updated values ([fe5a562](https://gitlab.com/hestia-earth/hestia-engine-models/commit/fe5a5622293b3cc699267fbbf5f6881214995de7))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.4...v0.0.5) (2021-04-16)


### Bug Fixes

* **utils:** remove default values for blank nodes ([2aa5884](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2aa5884806df25bdeb016b2541306eba23afd1c3))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.3...v0.0.4) (2021-04-16)


### Features

* **models:** improve debugging of lookup data ([4a4b672](https://gitlab.com/hestia-earth/hestia-engine-models/commit/4a4b672b27567e84d503a7a336f40ba1e1fe680f))


### Bug Fixes

* **cycle product:** should return new product not exising one ([6bb468e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/6bb468e025f9cff301cfe59dd8942818cd822845))
* **site:** fix `measurement.value` model modying the site ([0a97c93](https://gitlab.com/hestia-earth/hestia-engine-models/commit/0a97c937e8ae9fc45f3708431ff40ac37bdcaec3))
* **spatial:** handle no run with only `region` or `country` ([51ec51a](https://gitlab.com/hestia-earth/hestia-engine-models/commit/51ec51a5e3e7de098bd571150ca1ac965997d933))
* **spatial:** update term_id for `ecoClimateZone` ([8ce319f](https://gitlab.com/hestia-earth/hestia-engine-models/commit/8ce319facda7de0550e52eb270fd7cc25bd78392))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.2...v0.0.3) (2021-04-15)


### Bug Fixes

* **utils:** fix run in serie missing return ([63c4bda](https://gitlab.com/hestia-earth/hestia-engine-models/commit/63c4bdae50f22f929206ddaf5840f81442461aab))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-engine-models/compare/v0.0.1...v0.0.2) (2021-04-14)


### Features

* **agribalyse2016:** add `machineryInfrastructureDepreciatedAmountPerCycle` model ([d89398e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/d89398e9e118992ad7b2943c15000aecf422482c))
* **cycle:** add `dataCompleteness` model ([335628e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/335628ef5ac4a72d8d4c495c3696db54d1dfb51f))
* **cycle:** add `irrigated` model ([3d3fafd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3d3fafd11659ee744cfa91b2fc7b6bb6cf479a03))
* **cycle:** add `startDate` model ([e5cad0d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/e5cad0dc60df99924040b5b8bc1fb006f0306da2))
* **cycle:** add pre/post site models ([45ee629](https://gitlab.com/hestia-earth/hestia-engine-models/commit/45ee629e9e605df010fbe868321107ed6e8d86e5))
* **cycle:** add product models ([2b8446e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2b8446eb0c8e4868f39a202143697a7788afab75))
* **faostat2018:** add `seed` model ([602c8a8](https://gitlab.com/hestia-earth/hestia-engine-models/commit/602c8a8f4bd3c764a1b63e1f9a7aefa548426eab))
* **globalCropWaterModel2008:** add `rootingDepth` model ([20fc09d](https://gitlab.com/hestia-earth/hestia-engine-models/commit/20fc09d57374532147ac25084d1918c3852c1ec0))
* **impact assessment:** download cycle.site in pre-checks ([545c261](https://gitlab.com/hestia-earth/hestia-engine-models/commit/545c261cc38cd5897e8391a6306c2dc128ed5b09))
* **impact assessment:** import models from gap-filling engine ([2ed9f1e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/2ed9f1e8bf5a5bef772d4e3cf02d0462bc18a8a8))
* **ipcc2006Tier1:** add `aboveGroundCropResidue` model ([23cca27](https://gitlab.com/hestia-earth/hestia-engine-models/commit/23cca2763b7f0dfe55c3f6ad5723b106af477c78))
* **ipcc2006Tier1:** add `aboveGroundCropResidueRemoved` model ([314cdac](https://gitlab.com/hestia-earth/hestia-engine-models/commit/314cdacd1a2ede79626df079e282f5b66e76cda6))
* **ipcc2006Tier1:** add `aboveGroundCropResidueTotal` model ([3d4a15e](https://gitlab.com/hestia-earth/hestia-engine-models/commit/3d4a15e2fafee073cf8abef81e688542604ca129))
* **ipcc2006Tier1:** add `belowGroundCropResidue` model ([24ae0cd](https://gitlab.com/hestia-earth/hestia-engine-models/commit/24ae0cddd9bfd110db0c7c010db5c41ab9220324))
* **ipcc2006Tier1:** add `residue` model ([eeb9f5b](https://gitlab.com/hestia-earth/hestia-engine-models/commit/eeb9f5b27712f537051544e2a4b2c8903d71379f))
* **ipcc2019Tier1:** add `nitrogenContent` model ([a788c15](https://gitlab.com/hestia-earth/hestia-engine-models/commit/a788c1541045ee52443cd458f6139cc4b1087453))
* **pooreNemecek2018:** add `organicFertilizerToKgOrMass` model ([9a90080](https://gitlab.com/hestia-earth/hestia-engine-models/commit/9a900808df7b1ab4c8f416f3f1be5bde28530c0f))
* **site:** add empty pre/post checks ([1ed0e89](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1ed0e898cf27848c229a2f0dee97c0e52e385dd6))
* **spatial:** add `histosol` model ([1efcaa1](https://gitlab.com/hestia-earth/hestia-engine-models/commit/1efcaa1ce23c0ad4ee3c8b48f768198a66905b23))


### Bug Fixes

* **spatial:** add missing scale for `soilTotalNitrogenContent` ([05975b6](https://gitlab.com/hestia-earth/hestia-engine-models/commit/05975b6e634d44ca09f4a6b8ba212505c0edac93))

### 0.0.1 (2021-04-14)
