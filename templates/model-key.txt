from hestia_earth.models.log import logRequirements, logShouldRun
from .. import MODEL

# TODO: define requirements
REQUIREMENTS = {
    "_required_type_": {

    }
}
RETURNS = {
    "_return_type_": [{
        "_model_key_": ""
    }]
}
MODEL_KEY = '_model_key_'


def _run(_required_type_lowercase_: dict):
    # TODO: calculate value and return data
    value = 0


def _should_run(_required_type_lowercase_: dict):
    logRequirements(_required_type_lowercase_, model=MODEL, key=MODEL_KEY,
                    )
    logShouldRun(_required_type_lowercase_, MODEL, None, should_run, key=MODEL_KEY)
    return should_run


def run(_required_type_lowercase_: dict):
  return _run(_required_type_lowercase_) if _should_run(_required_type_lowercase_) else []
