import sys
import json
from hestia_earth.models.mocking.mock_search import RESULTS_PATH, create_search_results


def main(args: list):
    filepath = args[0] if len(args) > 0 else RESULTS_PATH
    data = create_search_results()
    with open(filepath, 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
