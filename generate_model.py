import os
import json
import argparse
from hestia_earth.schema import SchemaType, NodeType, EmissionMethodTier

SRC_DIR = os.path.join('hestia_earth', 'models')
TESTS_DIR = os.path.join('tests', 'models')
FIXTURES_DIR = os.path.join('tests', 'fixtures')
TEMPLATES_DIR = 'templates'

SKIP_RETURN_TYPES = [
    SchemaType.ACTOR.value,
    SchemaType.BIBLIOGRAPHY.value,
    SchemaType.TERM.value
] + [e.value for e in NodeType]

parser = argparse.ArgumentParser()

parser.add_argument('--model', type=str, required=True,
                    help='The name of the model. Usually an existing Term @id, but can also be cycle, site, etc.')
parser.add_argument('--required-type', type=str, required=True,
                    choices=[
                        SchemaType.CYCLE.value,
                        SchemaType.SITE.value,
                        SchemaType.IMPACTASSESSMENT.value,
                        SchemaType.TRANSFORMATION.value
                    ],
                    help='The type of the Node required to run the model.')
parser.add_argument('--return-type', type=str, required=True,
                    choices=[e.value for e in SchemaType if e.value not in SKIP_RETURN_TYPES],
                    help='The type of the Node returned by the model.')
parser.add_argument('--term-id', type=str,
                    help='The Term @id.')
parser.add_argument('--key', type=str,
                    help='A key for the new model if not using a Term.')
parser.add_argument('--method-tier', type=str,
                    choices=[e.value for e in EmissionMethodTier],
                    help='If the new model is for an Emission, the tier needs to be set.')

args = parser.parse_args()


def _get_model_template_path():
    test_paths = [
        os.path.join(TEMPLATES_DIR, f"model-{args.return_type.lower()}.txt"),
        os.path.join(TEMPLATES_DIR, 'model-term.txt') if args.term_id else '',
        os.path.join(TEMPLATES_DIR, 'model-key.txt') if args.key else ''
    ]
    return next((p for p in test_paths if os.path.exists(p)), None)


def _get_test_template_path():
    test_paths = [
        os.path.join(TEMPLATES_DIR, f"test-{args.return_type.lower()}.txt"),
        os.path.join(TEMPLATES_DIR, 'test-term.txt') if args.term_id else '',
        os.path.join(TEMPLATES_DIR, 'test-key.txt') if args.key else ''
    ]
    return next((p for p in test_paths if os.path.exists(p)), None)


def _get_fixture_template_path():
    test_paths = [
        os.path.join(TEMPLATES_DIR, f"fixture-{args.return_type.lower()}.json"),
        os.path.join(TEMPLATES_DIR, 'fixture-term.json') if args.term_id else '',
        os.path.join(TEMPLATES_DIR, 'fixture-key.json') if args.key else ''
    ]
    return next((p for p in test_paths if os.path.exists(p)), None)


def _template_content(template_path: str):
    print('Generating file using template', template_path)
    template = open(template_path, 'r').read()

    template = template.replace("_model_", args.model)

    template = template.replace("_required_type_lowercase_", args.required_type.lower())
    template = template.replace("_required_type_uppercase_", args.required_type.upper())
    template = template.replace("_required_type_", args.required_type)

    template = template.replace("_return_type_lowercase_", args.return_type.lower())
    template = template.replace("_return_type_uppercase_", args.return_type.upper())
    template = template.replace("_return_type_", args.return_type)

    template = template.replace("_term_id_", args.term_id or '')

    method_tier_converted = (args.method_tier or '').replace(' ', '_')
    template = template.replace("_method_tier_lowercase_", method_tier_converted.lower())
    template = template.replace("_method_tier_uppercase_", method_tier_converted.upper())
    template = template.replace("_method_tier_", args.method_tier or '')

    template = template.replace("_model_key_", args.key or '')

    return template


def _init_model(model_folder: str):
    is_src = model_folder.startswith(SRC_DIR)
    filepath = os.path.join(model_folder, '__init__.py')
    if not os.path.exists(filepath):
        with open(filepath, 'w') as f:
            content = _template_content(os.path.join(TEMPLATES_DIR, 'model-model.txt')) if is_src else ''
            f.write(content)


def _model_folder(folder: str):
    model_folder = os.path.join(folder, args.model)
    os.makedirs(model_folder, exist_ok=True)
    _init_model(model_folder)
    return model_folder


def _filename(): return f"{args.term_id or args.key.replace('.', '/')}.py"


def _write_model(content: str):
    filepath = os.path.join(_model_folder(SRC_DIR), _filename())
    print('Generate model content in', filepath)
    with open(filepath, 'w') as f:
        f.write(content)


def _write_test(content: str):
    filepath = os.path.join(_model_folder(TESTS_DIR), f"test_{_filename()}")
    print('Generate test content in', filepath)
    with open(filepath, 'w') as f:
        f.write(content)

    fixtures_folder = os.path.join(_model_folder(FIXTURES_DIR), args.term_id or args.key.replace('.', '/'))
    os.makedirs(fixtures_folder, exist_ok=True)
    print('Generate fixtures in', fixtures_folder)

    node_template_path = os.path.join(TEMPLATES_DIR, f"fixture-{args.required_type.lower()}.json")
    node_content = _template_content(node_template_path) if os.path.exists(node_template_path) else json.dumps({
        "id": "test-id",
        "type": args.required_type
    }, indent=2)
    with open(os.path.join(fixtures_folder, f"{args.required_type.lower()}.jsonld"), 'w') as f:
        f.write(node_content)

    with open(os.path.join(fixtures_folder, 'result.jsonld'), 'w') as f:
        f.write(_template_content(_get_fixture_template_path()))


def main():
    term_id = args.term_id

    if args.return_type == SchemaType.EMISSION.value:
        if not term_id:
            raise Exception('Please provide --term-id for new Emission model.')
        if not args.method_tier:
            raise Exception('Please provide --method-tier for new Emission model.')

    _write_model(_template_content(_get_model_template_path()))
    _write_test(_template_content(_get_test_template_path()))


if __name__ == "__main__":
    main()
