#!/bin/sh

docker build --progress=plain \
  -t hestia-engine-models:latest \
  .

docker run --rm \
  --name hestia-engine-models \
  -v ${PWD}:/app \
  hestia-engine-models:latest python run.py "$@"
