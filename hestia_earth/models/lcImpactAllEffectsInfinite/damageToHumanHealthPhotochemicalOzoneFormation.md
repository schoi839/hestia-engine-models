## Damage to human health, photochemical ozone formation

The disability-adjusted life years lost in the human population due to photochemical ozone formation. See [lc-impact.eu](https://lc-impact.eu/HHphotochemical_ozone_formation.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToHumanHealthPhotochemicalOzoneFormation](https://hestia.earth/term/damageToHumanHealthPhotochemicalOzoneFormation)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffectsInfinite](https://hestia.earth/term/lcImpactAllEffectsInfinite)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-emission-OzoneFormationDamageToHumanHealthLCImpactCF.csv](https://hestia.earth/glossary/lookups/region-emission-OzoneFormationDamageToHumanHealthLCImpactCF.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffectsInfinite import run

print(run('damageToHumanHealthPhotochemicalOzoneFormation', ImpactAssessment))
```
