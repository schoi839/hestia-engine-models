## Damage to human health, stratospheric ozone depletion

The disability-adjusted life years lost in the human population due to stratospheric ozone depletion. See [lc-impact.eu](https://lc-impact.eu/HHozone_depletion.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToHumanHealthStratosphericOzoneDepletion](https://hestia.earth/term/damageToHumanHealthStratosphericOzoneDepletion)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffectsInfinite](https://hestia.earth/term/lcImpactAllEffectsInfinite)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `dalyAllEffectsInfiniteStratosphericOzoneDepletionDamageToHumanHealthLCImpact`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffectsInfinite import run

print(run('damageToHumanHealthStratosphericOzoneDepletion', ImpactAssessment))
```
