# Blonk Consultants (2016)

This model calculates the land transformation and emissions related to land use change, using the Blonk Consultants (2016) direct land use change assessment model.
