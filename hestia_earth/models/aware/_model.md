# AWARE

This model characterises water use based on the geospatial AWARE model (see UNEP (2016); Boulay et al (2016); Boulay et al (2020); EC-JRC (2017)).
