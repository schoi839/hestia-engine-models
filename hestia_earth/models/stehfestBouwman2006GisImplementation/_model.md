# Stehfest Bouwman (2006) GIS Implementation

This model calculates the direct N2O and NOx emissions due to the use of fertiliser, by creating a country-average version of the Stehfest & Bouwman (2006, Nutrient Cycling in Agroecosystems, 74,  207–228) model using GIS software.
