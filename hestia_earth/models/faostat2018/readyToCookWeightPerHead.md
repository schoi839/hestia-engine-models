## Ready-to-cook weight per head

The ready-to-cook weight of the animals per head.

### Returns

* A list of [Products](https://hestia.earth/schema/Products) with:
  - a list of [properties](https://hestia.earth/schema/Products#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [readyToCookWeightPerHead](https://hestia.earth/term/readyToCookWeightPerHead)
    - [methodModel](https://hestia.earth/schema/Property#methodModel) with [faostat2018](https://hestia.earth/term/faostat2018)
    - [value](https://hestia.earth/schema/Property#value)
    - [statsDefinition](https://hestia.earth/schema/Property#statsDefinition) with `regions`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalProduct](https://hestia.earth/glossary?termType=animalProduct) and [term](https://hestia.earth/schema/Product#term) of [units](https://hestia.earth/schema/Term#units) = `kg ready-to-cook weight`
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-animalProduct-animalProductGroupingFAO-productionQuantity.csv](https://hestia.earth/glossary/lookups/region-animalProduct-animalProductGroupingFAO-productionQuantity.csv) -> production quantity
- [region-animalProduct-animalProductGroupingFAO-head.csv](https://hestia.earth/glossary/lookups/region-animalProduct-animalProductGroupingFAO-head.csv) -> number of heads

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.faostat2018 import run

print(run('readyToCookWeightPerHead', Cycle))
```
