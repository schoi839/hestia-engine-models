# Environmental Footprint v3

This model characterises chemicals' toxicity according to the [USEtox model, version 2.1](https://usetox.org/model/download/usetox2.12), with slight modifications to build freshwater ecotoxicity characterisations from HC20-EC10eq values ([Owsianiak et al (2023)](https://doi.org/10.1016/j.chemosphere.2022.136807), [Sala et al (2022)](https://doi.org/10.1007/s11367-022-02033-0)). The HC20-EC10eq value represents the hazardous concentration of a chemical at which 20% of the species considered are exposed to a concentration above their EC10.
