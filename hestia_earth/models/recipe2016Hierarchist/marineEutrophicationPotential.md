## Marine eutrophication potential

The potential of nutrient emissions to cause excessive growth of aquatic plants and algae in marine ecosystems (e.g. seas, oceans, estuaries). Some algae - particularly phytoplankton - are inedible to much other aquatic life and go uneaten, meaning the phytoplankton die and get broken down by bacteria which use oxygen for respiration. This oxygen demand depltes oxygen in the water leading to hypoxia. Marine eutrophication is primarily linked to nitrogen as this tends to be the limiting nutrient in these ecosystems.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [marineEutrophicationPotential](https://hestia.earth/term/marineEutrophicationPotential)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Hierarchist](https://hestia.earth/term/recipe2016Hierarchist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `nEqHierarchistMarineEutrophicationReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Hierarchist import run

print(run('marineEutrophicationPotential', ImpactAssessment))
```
