## Temperature (monthly)

The average monthly air temperature, averaged over each day in the month.

Compute Monthly value based on Daily values.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [temperatureMonthly](https://hestia.earth/term/temperatureMonthly)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [dates](https://hestia.earth/schema/Measurement#dates)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `modelled`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` **or** `agri-food processor`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - Data completeness assessment for id: [term.id](https://hestia.earth/schema/Completeness#id) must be `temperatureDaily`

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('temperatureMonthly', Site))
```
