## Product Currency

Converts all the currencies to `USD` using historical rates.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [currency](https://hestia.earth/schema/Product#currency) with `USD`
  - [price](https://hestia.earth/schema/Product#price) with `in USD`
  - [revenue](https://hestia.earth/schema/Product#revenue) with `in USD`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [price](https://hestia.earth/schema/Product#price) and [currency](https://hestia.earth/schema/Product#currency) with `not in USD`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.currency', Cycle))
```
