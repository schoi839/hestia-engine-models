## Residue removed

The share of above ground crop residue removed.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [residueRemoved](https://hestia.earth/term/residueRemoved)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) and [value](https://hestia.earth/schema/Product#value) `> 0`
    - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved) and [value](https://hestia.earth/schema/Product#value) `> 0`
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`

### Lookup used

- [cropResidueManagement.csv](https://hestia.earth/glossary/lookups/cropResidueManagement.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('residueRemoved', Cycle))
```
