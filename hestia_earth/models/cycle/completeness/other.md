## Completeness Other

This model checks if we have the requirements below and updates the
[Data Completeness](https://hestia.earth/schema/Completeness#other) value.

### Returns

* A [Completeness](https://hestia.earth/schema/Completeness) with:
  - [other](https://hestia.earth/schema/Completeness#other)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for other: [completeness.other](https://hestia.earth/schema/Completeness#other) must be `False`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [seed](https://hestia.earth/term/seed) **or** [saplings](https://hestia.earth/term/saplings)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover`

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `isPlantation`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('completeness.other', Cycle))
```
