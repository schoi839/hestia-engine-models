## Completeness Material

This model checks if we have the requirements below and updates the
[Data Completeness](https://hestia.earth/schema/Completeness#material) value.

### Returns

* A [Completeness](https://hestia.earth/schema/Completeness) with:
  - [material](https://hestia.earth/schema/Completeness#material)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [machineryInfrastructureDepreciatedAmountPerCycle](https://hestia.earth/term/machineryInfrastructureDepreciatedAmountPerCycle)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('completeness.material', Cycle))
```
