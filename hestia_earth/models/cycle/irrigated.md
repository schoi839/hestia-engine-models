## Irrigated

More than 25 mm of irrigation per year (250 m3/ha). The area irrigated can be specified as a percentage.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [irrigated](https://hestia.earth/term/irrigated)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - Data completeness assessment for water: [completeness.water](https://hestia.earth/schema/Completeness#water)
  - [functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) with `1 ha`
  - none of:
    - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
      - [units](https://hestia.earth/schema/Practice#units) with `%` **or** `% area` and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [waterRegime](https://hestia.earth/glossary?termType=waterRegime)
  - optional:
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water) and [value](https://hestia.earth/schema/Input#value)

### Lookup used

- [waterRegime.csv](https://hestia.earth/glossary/lookups/waterRegime.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('irrigated', Cycle))
```
