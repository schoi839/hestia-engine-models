## Above ground crop residue, total

The total amount of above ground crop residue as dry matter. This total is the value prior to crop residue management practices (for example, burning or removal). Properties can be added, such as the nitrogen content. The amount of discarded crop is not included and should be recorded separately.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal)
  - [value](https://hestia.earth/schema/Product#value)
  - [statsDefinition](https://hestia.earth/schema/Product#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [residueRemoved](https://hestia.earth/term/residueRemoved)

### Lookup used

- [cropResidue.csv](https://hestia.earth/glossary/lookups/cropResidue.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('aboveGroundCropResidueTotal', Cycle))
```
