## Damage to resource availability

The surplus costs of future resource production in 2013 US Dollars over an infinitive timeframe, assuming constant annual production and a 3% discount rate. See [ReCiPe 2016](https://pre-sustainability.com/legacy/download/Report_ReCiPe_2017.pdf).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToResourceAvailability](https://hestia.earth/term/damageToResourceAvailability)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Individualist](https://hestia.earth/term/recipe2016Individualist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for electricityFuel: [completeness.electricityFuel](https://hestia.earth/schema/Completeness#electricityFuel) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?termType=fuel)

### Lookup used

- [fuel.csv](https://hestia.earth/glossary/lookups/fuel.csv) -> `usd2013IndividualistDamageToResourceAvailabilityReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Individualist import run

print(run('damageToResourceAvailability', ImpactAssessment))
```
