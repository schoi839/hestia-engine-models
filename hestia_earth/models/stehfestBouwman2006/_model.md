# Stehfest Bouwman (2006)

This model calculates the direct N2O and NOx emissions due to the use of fertiliser using the regression model detailed in Stehfest & Bouwman (2006, Nutrient Cycling in Agroecosystems, 74,  207–228). It takes data on factors including soil, climate, and crop type. Here we also extend it to crop residue and animal excreta deposited directly on pasture.
