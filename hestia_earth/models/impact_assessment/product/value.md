## Product Value

Returns the [value](https://hestia.earth/schema/Product#value) of the Product linked to the Cycle.

### Returns

* A [Product](https://hestia.earth/schema/Product) with:
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [product](https://hestia.earth/schema/ImpactAssessment#product) with:
    - a [term](https://hestia.earth/schema/Product#term) and none of:
      - [value](https://hestia.earth/schema/Product#value)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [value](https://hestia.earth/schema/Product#value)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('product.value', ImpactAssessment))
```
