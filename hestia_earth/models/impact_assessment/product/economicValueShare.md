## Product Economic Value Share

Returns the [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) of the Product
linked to the Cycle.

### Returns

* A [Product](https://hestia.earth/schema/Product) with:
  - [economicValueShare](https://hestia.earth/schema/Product#economicValueShare)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [product](https://hestia.earth/schema/ImpactAssessment#product) with:
    - a [term](https://hestia.earth/schema/Product#term) and none of:
      - [economicValueShare](https://hestia.earth/schema/Product#economicValueShare)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [economicValueShare](https://hestia.earth/schema/Product#economicValueShare)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('product.economicValueShare', ImpactAssessment))
```
