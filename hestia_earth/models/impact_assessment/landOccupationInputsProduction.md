## Land occupation, inputs production

The amount of land required to produce the inputs used by the Cycle, multiplied by the time (in years) that the land was occupied including fallow periods.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landOccupationInputsProduction](https://hestia.earth/term/landOccupationInputsProduction)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('landOccupationInputsProduction', ImpactAssessment))
```
