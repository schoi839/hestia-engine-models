## Impact Assessment Post Checks Cycle

This model is run only if the [pre model](../pre_checks/cycle.md) has been run before.
This model will restore the `impactAssessment.cycle` as a "linked node"
(i.e. it will be set with only `@type`, `@id` and `name` keys).

### Returns

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - [@id](https://hestia.earth/schema/Cycle#id) must be set (is linked to an existing Cycle)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run(ImpactAssessment))
```
