## Impact Assessment Post Checks Site

This model is run only if the [pre model](../pre_checks/site.md) has been run before.
This model will restore the `impactAssessment.site` as a "linked node"
(i.e. it will be set with only `@type`, `@id` and `name` keys).

### Returns

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [@id](https://hestia.earth/schema/Site#id) must be set (is linked to an existing Site)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run(ImpactAssessment))
```
