## Residue

Re-scale all [crop residue management](https://hestia.earth/glossary?termType=cropResidueManagement) Practices
to make sure they all add up to 100%. Note: only practices added by Hestia will be recalculated.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [methodModel](https://hestia.earth/schema/Practice#methodModel) with [koble2014](https://hestia.earth/term/koble2014)
  - [value](https://hestia.earth/schema/Practice#value)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [term](https://hestia.earth/schema/Practice#term) with [residueBurnt](https://hestia.earth/term/residueBurnt) **or** [residueLeftOnField](https://hestia.earth/term/residueLeftOnField) **or** [residueRemoved](https://hestia.earth/term/residueRemoved) and [added](https://hestia.earth/schema/Practice#added) with `value` and Data completeness assessment for @id: [model.@id](https://hestia.earth/schema/Completeness#@id) must be `koble2014`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.koble2014 import run

print(run('cropResidueManagement', Cycle))
```
