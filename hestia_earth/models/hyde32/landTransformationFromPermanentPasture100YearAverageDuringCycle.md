## Land transformation, from permanent pasture, 100 year average, during Cycle

The amount of land used by this Cycle, that changed use from permanent pasture to the current use in the last 100 years, divided by 100.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromPermanentPasture100YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromPermanentPasture100YearAverageDuringCycle)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [hyde32](https://hestia.earth/term/hyde32)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `animal housing` **or** `pond` **or** `agri-food processor` **or** `food retailer`
  - either:
    - a [country](https://hestia.earth/schema/ImpactAssessment#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
    - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - [endDate](https://hestia.earth/schema/ImpactAssessment#endDate)
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - either:
      - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
        - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
        - a list of [products](https://hestia.earth/schema/Cycle#products) with:
          - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
      - for plantations, additional properties are required:
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDensity](https://hestia.earth/term/nurseryDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationProductiveLifespan](https://hestia.earth/term/plantationProductiveLifespan)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationDensity](https://hestia.earth/term/plantationDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationLifespan](https://hestia.earth/term/plantationLifespan)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

One of (depending on `site.siteType`):

- [region-cropland-landTransformation100years.csv](https://hestia.earth/glossary/lookups/region-cropland-landTransformation100years.csv) -> `permanent pasture`
- [region-forest-landTransformation100years.csv](https://hestia.earth/glossary/lookups/region-forest-landTransformation100years.csv) -> `permanent pasture`
- [region-other_natural_vegetation-landTransformation100years.csv](https://hestia.earth/glossary/lookups/region-other_natural_vegetation-landTransformation100years.csv) -> `permanent pasture`
- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.hyde32 import run

print(run('landTransformationFromPermanentPasture100YearAverageDuringCycle', ImpactAssessment))
```
