## Transport Value

This model calculates the `distance` of the [Transport](https://hestia.earth/schema/Transport) linked to the
[Inputs of the Cycle](https://hestia.earth/schema/Cycle#inputs) by calculating the distance between the
country of the Cycle and the country of origin of the Input (which must be different).

### Returns

* A list of [Transports](https://hestia.earth/schema/Transport) with:
  - [methodModel](https://hestia.earth/schema/Transport#methodModel) with [haversineFormula](https://hestia.earth/term/haversineFormula)
  - [distance](https://hestia.earth/schema/Transport#distance)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - a [country](https://hestia.earth/schema/Input#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region) and a list of [transports](https://hestia.earth/schema/Input#transport)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.haversineFormula import run

print(run('transport.distance', Cycle))
```
