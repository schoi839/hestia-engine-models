## Damage to freshwater ecosystems, freshwater ecotoxicity

The fraction of species richness that may be potentially lost in freshwater ecosystems due to freshwater ecotoxicity. See [lc-impact.eu](https://lc-impact.eu/EQfreshwater_ecotoxicity.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToFreshwaterEcosystemsFreshwaterEcotoxicity](https://hestia.earth/term/damageToFreshwaterEcosystemsFreshwaterEcotoxicity)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffects100Years](https://hestia.earth/term/lcImpactAllEffects100Years)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for pesticidesAntibiotics: [completeness.pesticidesAntibiotics](https://hestia.earth/schema/Completeness#pesticidesAntibiotics) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [pesticideAI](https://hestia.earth/glossary?termType=pesticideAI)

### Lookup used

- [pesticideAI.csv](https://hestia.earth/glossary/lookups/pesticideAI.csv) -> `pdfYear100YearsFreshwaterEcotoxicityDamageToFreshwaterEcosystemsLCImpact`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffects100Years import run

print(run('damageToFreshwaterEcosystemsFreshwaterEcotoxicity', ImpactAssessment))
```
