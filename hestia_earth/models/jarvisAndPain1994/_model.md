# Jarvis and Pain (1994)

This model, described in [Jarvis and Pain (1994)](https://doi.org/10.1007/BF01098471), estimates the N2 emissions as a ratio of N2O emissions in excreta management systems.
