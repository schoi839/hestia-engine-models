from hestia_earth.models.utils.constant import Units, get_atomic_conversion
from hestia_earth.utils.model import find_term_match
from hestia_earth.utils.tools import list_sum
from hestia_earth.schema import EmissionMethodTier, EmissionStatsDefinition
from . import MODEL
from hestia_earth.models.log import logRequirements, logShouldRun
from hestia_earth.models.utils.emission import _new_emission

REQUIREMENTS = {
    "Cycle": {
        "emissions": [
            {"@type": "Emission", "value": "", "term.@id": "n2OToAirExcretaDirect"}
        ]
    }
}
RETURNS = {
    "Emission": [{
        "value": "",
        "methodTier": "tier 1",
        "statsDefinition": "modelled"
    }]
}
TERM_ID = 'n2ToAirExcreta'
TIER = EmissionMethodTier.TIER_1.value
N2O_TERM_ID = 'n2OToAirExcretaDirect'


def _emission(value: float):
    emission = _new_emission(TERM_ID, MODEL)
    emission['value'] = [value]
    emission['methodTier'] = TIER
    emission['statsDefinition'] = EmissionStatsDefinition.MODELLED.value
    return emission


def _run(n2o: dict):
    value = 3 * list_sum(n2o.get("value", [])) / get_atomic_conversion(Units.KG_N2O, Units.TO_N)
    return [_emission(value)]


def _should_run(cycle: dict):
    n2o = find_term_match(cycle.get('emissions', []), N2O_TERM_ID)

    logRequirements(cycle, model=MODEL, term=TERM_ID, has_n2o=n2o is not None)

    should_run = all([n2o])
    logShouldRun(cycle, MODEL, TERM_ID, should_run, methodTier=TIER)
    return should_run, n2o


def run(cycle: dict):
    should_run, n2o = _should_run(cycle)
    return _run(n2o) if should_run else []
