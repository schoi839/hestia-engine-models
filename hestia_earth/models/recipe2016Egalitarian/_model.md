# ReCiPe 2016 Egalitarian

These models characterise emissions and resource use according to the ReCiPe 2016 method, using an egalitarian perspective (see [Huijbregts et al (2016)](https://www.rivm.nl/bibliotheek/rapporten/2016-0104.pdf), [WMO (2011)](https://ozone.unep.org/sites/default/files/2019-05/00-SAP-2010-Assement-report.pdf); [Hayashi et al. (2006)](https://doi.org/10.1065/lca2004.11.189); [De Schryver et al. (2011)](https://doi.org/10.1111/j.1530-9290.2011.00371.x)).
