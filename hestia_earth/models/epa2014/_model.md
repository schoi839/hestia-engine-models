# EPA (2014)

These models calculate direct and indirect greenhouse gas emissions using the methodology detailed in the EPA (2014) guidelines, [EPA (2014) guidelines](https://www.epa.gov/sites/default/files/2015-12/documents/us-ghg-inventory-2014-annexes.pdf).
