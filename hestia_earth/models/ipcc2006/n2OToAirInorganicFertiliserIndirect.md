## N2O, to air, inorganic fertiliser, indirect

Nitrous oxide emissions to air, indirectly created from NOx, NH3, and NO3 emissions, from inorganic fertiliser.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirInorganicFertiliserIndirect](https://hestia.earth/term/n2OToAirInorganicFertiliserIndirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2006](https://hestia.earth/term/ipcc2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser) must be `True`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser) and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
  - a list of [emissions](https://hestia.earth/schema/Cycle#emissions) with:
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [no3ToGroundwaterInorganicFertiliser](https://hestia.earth/term/no3ToGroundwaterInorganicFertiliser)
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirInorganicFertiliser](https://hestia.earth/term/nh3ToAirInorganicFertiliser)
    - [value](https://hestia.earth/schema/Emission#value) and [term](https://hestia.earth/schema/Emission#term) with [noxToAirInorganicFertiliser](https://hestia.earth/term/noxToAirInorganicFertiliser)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2006 import run

print(run('n2OToAirInorganicFertiliserIndirect', Cycle))
```
