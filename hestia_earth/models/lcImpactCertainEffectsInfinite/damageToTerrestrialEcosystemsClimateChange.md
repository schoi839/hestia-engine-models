## Damage to terrestrial ecosystems, climate change

The fraction of species richness that may be potentially lost in terrestrial ecosystems due to climate change. See [lc-impact.eu]( https://lc-impact.eu/EQclimate_change.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsClimateChange](https://hestia.earth/term/damageToTerrestrialEcosystemsClimateChange)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactCertainEffectsInfinite](https://hestia.earth/term/lcImpactCertainEffectsInfinite)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `pdfYearsCertainEffectsInfiniteClimateChangeDamageToTerrestrialEcosystemsLCImpact`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactCertainEffectsInfinite import run

print(run('damageToTerrestrialEcosystemsClimateChange', ImpactAssessment))
```
