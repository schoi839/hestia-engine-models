## Damage to freshwater ecosystems (PDF*year)

The fraction of freshwater species that are commited to become globally extinct over a certain period of time if the pressure continues to happen. See [lc-impact.eu](https://lc-impact.eu/ecosystem_quality.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToFreshwaterEcosystemsPdfYear](https://hestia.earth/term/damageToFreshwaterEcosystemsPdfYear)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactCertainEffectsInfinite](https://hestia.earth/term/lcImpactCertainEffectsInfinite)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [impacts](https://hestia.earth/schema/ImpactAssessment#impacts) with:
    - [value](https://hestia.earth/schema/Indicator#value) and a [methodModel](https://hestia.earth/schema/Indicator#methodModel) with:
      - [@id](https://hestia.earth/schema/Term#id) must be set (is linked to an existing Term)

### Lookup used

- [characterisedIndicator.csv](https://hestia.earth/glossary/lookups/characterisedIndicator.csv) -> `pdfYearsDamageToFreshwaterEcosystemsLCImpact`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactCertainEffectsInfinite import run

print(run('damageToFreshwaterEcosystemsPdfYear', ImpactAssessment))
```
