## Nitrogen content

The total nitrogen content of something, as N, expressed as a percentage.

### Returns

* A list of [Products](https://hestia.earth/schema/Products) with:
  - a list of [properties](https://hestia.earth/schema/Products#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
    - [methodModel](https://hestia.earth/schema/Property#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
    - [value](https://hestia.earth/schema/Property#value)
    - [statsDefinition](https://hestia.earth/schema/Property#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) and [value](https://hestia.earth/schema/Product#value) `> 0` and optional:
      - a list of [properties](https://hestia.earth/schema/Product#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [dryMatter](https://hestia.earth/term/dryMatter)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `IPCC_2019_Ratio_AGRes_YieldDM`; `IPCC_2019_Ratio_BGRes_AGRes`; `N_Content_AG_Residue`; `N_Content_BG_Residue`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('nitrogenContent', Cycle))
```
