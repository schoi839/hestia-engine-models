## CH4, to air, enteric fermentation

Methane emissions to air, from enteric fermentation by ruminants.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [ch4ToAirEntericFermentation](https://hestia.earth/term/ch4ToAirEntericFermentation)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [sd](https://hestia.earth/schema/Emission#sd)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 2`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [liveAnimal](https://hestia.earth/glossary?termType=liveAnimal) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [feedFoodAdditive](https://hestia.earth/glossary?termType=feedFoodAdditive) **or** [forage](https://hestia.earth/glossary?termType=forage) **or** [liveAquaticSpecies](https://hestia.earth/glossary?termType=liveAquaticSpecies) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` and [value](https://hestia.earth/schema/Input#value) `> 0` and [isAnimalFeed](https://hestia.earth/schema/Input#isAnimalFeed) with `True` and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [neutralDetergentFibreContent](https://hestia.earth/term/neutralDetergentFibreContent) **or** [energyContentHigherHeatingValue](https://hestia.earth/term/energyContentHigherHeatingValue)

### Lookup used

- [liveAnimal.csv](https://hestia.earth/glossary/lookups/liveAnimal.csv) -> `digestibility`; `percentageYmMethaneConversionFactorEntericFermentationIPCC2019`; `percentageYmMethaneConversionFactorEntericFermentationIPCC2019-sd`
- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `neutralDetergentFibreContent`; `energyContentHigherHeatingValue`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `productTermIdsAllowed`; `productTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('ch4ToAirEntericFermentation', Cycle))
```
