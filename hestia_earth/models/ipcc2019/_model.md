# IPCC (2019)

These models, described in the [IPCC (2019)](https://www.ipcc.ch/report/2019-refinement-to-the-2006-ipcc-guidelines-for-national-greenhouse-gas-inventories/) refinement to the [IPCC (2006)](https://www.ipcc.ch/report/2006-ipcc-guidelines-for-national-greenhouse-gas-inventories/) guidelines, calculate direct and indirect greenhouse gas emissions and provide data for lookup tables.
