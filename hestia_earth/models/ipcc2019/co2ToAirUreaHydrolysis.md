## CO2, to air, urea hydrolysis

Carbon dioxide emissions to air, from urea hydrolysis (a corresponding negative emission occurs during fertiliser production).

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [co2ToAirUreaHydrolysis](https://hestia.earth/term/co2ToAirUreaHydrolysis)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`
  - [statsDefinition](https://hestia.earth/schema/Emission#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/null#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - either:
    - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser)
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser)
  - optional:
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) with [inorganicNitrogenFertiliserUnspecifiedKgN](https://hestia.earth/term/inorganicNitrogenFertiliserUnspecifiedKgN)

### Lookup used

- [inorganicFertiliser.csv](https://hestia.earth/glossary/lookups/inorganicFertiliser.csv) -> `Urea_UAS_Amm_Bicarb`; `UAN_Solu`; `CO2_urea_emissions_factor`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('co2ToAirUreaHydrolysis', Cycle))
```
