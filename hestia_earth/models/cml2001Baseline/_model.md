# CML2001 Baseline

These models characterise emissions and resource use according to the CML2001 Baseline method (see Guinée et al. 2002; Jenkin & Hayman, 1999; Derwent et al. 1998, Huijbregts, 1999).
