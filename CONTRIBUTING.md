# Contributing to Hestia Engine Models

## <a name="local-setup"></a> Local Development

To develop on this module, please follow these steps:

1. Install python `3.7` minimum
1. Optional: install NodeJS `12` minimum and then run `npm install`. This will allow you to run `npm test` and validate your fixtures files follow the [Hestia Schema](http://hestia.earth/schema).

## <a name="run-tests"></a> Running the tests

Note: the tests under `tests/models/spatial` cannot be run locally as they have external dependencies with private APIs.

1. Set up your environment variables: you will need 2 environment variables to run the tests, which you can setup using `export`:
    ```bash
    export API_URL=https://api.hestia.earth
    export WEB_URL=https://hestia.earth
    ```
1. Install pytest: `pip install pytest-cov`
1. Run `pytest` to run all tests or `pytest tests/models/agribalyse2016` to run a single model.

Note: if you have both python 2 and 3 installed, you may need to use `pip3` to install.

To use the development environment variables, please contact us at <community@hestia.earth>.

### <a name="run-tests-docker"></a> Run with Docker

You can run the tests using docker with the provided `run-docker-test.sh` script.

1. Setup your enviorment variables: you can either use `export` as described above or create a `.env` file with the following:
    ```
    API_URL=https://api.hestia.earth
    WEB_URL=https://www.hestia.earth
    ```
1. To run all the tests do:
    ```bash
    # you can ommit the environment prepend if you exported it
    API_URL=https://api.hestia.earth ./run-docker-test.sh
    ```
1. To run a single model do:
    ```bash
    # you can ommit the environment prepend if you exported it
    API_URL=https://api.hestia.earth ./run-docker-test.sh pytest tests/models/agribalyse2016
    ```

## <a name="layer"></a> Deploying the Lambda Layer

1. Connect to your AWS account
1. Build the layer files:
    ```bash
    ./layer/build.sh <API_URL>
    ```
1. Deploy the new layer version:
    ```bash
    ./layer/deploy.sh
    ```

# General Contribution Guidelines

We would love for you to contribute to Hestia and help make it even better than it is
today! As a contributor, here are the guidelines we would like you to follow:

 - [Issues and Bugs](#issue)
 - [Feature Requests](#feature)
 - [Submission Guidelines](#submit)
 - [Coding Rules](#rules)
 - [Commit Message Guidelines](#commit)

## <a name="issue"></a> Found a Bug?
If you find a bug in the source code, you can help us by
[submitting an issue](#submit-issue) to our [GitLab Repository][https://gitlab.com/hestia-earth/hestia-engine-models]. Even better, you can [submit a Merge Request](#submit-mr) with a fix.

## <a name="feature"></a> Missing a Feature?
You can *request* a new feature by [submitting an issue](#submit-issue) to our GitLab
Repository. If you would like to *implement* a new feature, please submit an issue with
a proposal for your work first, to be sure that we can use it.
Please consider what kind of change it is:

* For a **Major Feature**, first open an issue and outline your proposal so that it can be
discussed. This will also allow us to better coordinate our efforts, prevent duplication of work,
and help you to craft the change so that it is successfully accepted into the project.
* **Small Features** can be crafted and directly [submitted as a Merge Request](#submit-mr).

## <a name="submit"></a> Submission Guidelines

### <a name="submit-issue"></a> Submitting an Issue

Before you submit an issue, please search the issue tracker, maybe an issue for your problem already exists and the discussion might inform you of workarounds readily available.

We want to fix all the issues as soon as possible, but before fixing a bug we need to reproduce and confirm it. In order to reproduce bugs, we will systematically ask you to provide a minimal reproduction. Having a minimal reproducible scenario gives us a wealth of important information without going back & forth to you with additional questions.

A minimal reproduction allows us to quickly confirm a bug (or point out a coding problem) as well as confirm that we are fixing the right problem.

We will be insisting on a minimal reproduction scenario in order to save maintainers time and ultimately be able to fix more bugs. Interestingly, from our experience, users often find coding problems themselves while preparing a minimal reproduction. We understand that sometimes it might be hard to extract essential bits of code from a larger codebase but we really need to isolate the problem before we can fix it.

Unfortunately, we are not able to investigate / fix bugs without a minimal reproduction, so if we don't hear back from you, we are going to close an issue that doesn't have enough info to be reproduced.

### <a name="submit-mr"></a> Submitting a Merge Request (MR)
Before you submit your Merge Request (MR) consider the following guidelines:

1. Search [GitLab](https://gitlab.com/hestia-earth/hestia-engine-models/-/merge_requests) for an open or closed MR
  that relates to your submission. You don't want to duplicate effort.
1. Be sure that an issue describes the problem you're fixing, or documents the design for the feature you'd like to add.
  Discussing the design up front helps to ensure that we're ready to accept your work.
1. Fork the hestia-earth/hestia-engine-models repo.
1. Make your changes in a new git branch:

     ```shell
     git checkout -b my-fix-branch master
     ```

1. Create your patch, **including appropriate test cases**.
1. Follow our [Coding Rules](#rules).
1. Commit your changes using a descriptive commit message that follows our
  [commit message conventions](#commit). Adherence to these conventions
  is necessary because release notes are automatically generated from these messages.

     ```shell
     git commit -a
     ```
    Note: the optional commit `-a` command line option will automatically "add" and "rm" edited files.

1. Push your branch to GitLab:

    ```shell
    git push origin my-fix-branch
    ```

1. In GitLab, send a pull request to `schema:master`.
* If we suggest changes then:
  * Make the required updates.
  * Re-run the Hestia test suites to ensure tests are still passing.
  * Rebase your branch and force push to your GitLab repository (this will update your Merge Request):

    ```shell
    git rebase master -i
    git push -f
    ```

That's it! Thank you for your contribution!

#### After your pull request is merged

After your pull request is merged, you can safely delete your branch and pull the changes
from the main (upstream) repository:

* Delete the remote branch on GitLab either through the GitLab web UI or your local shell as follows:

    ```shell
    git push origin --delete my-fix-branch
    ```

* Check out the master branch:

    ```shell
    git checkout master -f
    ```

* Delete the local branch:

    ```shell
    git branch -D my-fix-branch
    ```

* Update your master with the latest upstream version:

    ```shell
    git pull --ff upstream master
    ```

## <a name="rules"></a> Coding Rules
To ensure consistency throughout the source code, keep these rules in mind as you are working:

* All features or bug fixes **must be tested** by one or more specs (unit-tests).
* All public API methods **must be documented**. (Details TBC).
* We follow [Google's JavaScript Style Guide][js-style-guide], but wrap all code at
  **100 characters**. An automated formatter is available, see
  [DEVELOPER.md](docs/DEVELOPER.md#clang-format).

## <a name="commit"></a> Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the Hestia change log**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier
to read on GitLab as well as in various git tools.

The footer should contain a [closing reference to an issue](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html) if any.

Samples: (even more [samples](https://gitlab.com/hestia-earth/hestia-engine-models/-/commits/develop))

```
docs(changelog): update changelog to beta.5
```
```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

### Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

### Type
Must be one of the following:

* **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
* **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests

### Scope
The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated from commit messages).

### Subject
The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.
