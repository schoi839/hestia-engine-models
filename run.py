# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Put your test cycle into the `samples` folder
# 3. Run `python run.py samples/cycle.jsonld`
from dotenv import load_dotenv
load_dotenv()


import sys
import logging
import json
import importlib
from hestia_earth.utils.tools import current_time_ms

from hestia_earth.models.log import logger


def _init_gee(model: str):
    from hestia_earth.models.geospatialDatabase import MODEL
    if model == MODEL:
        try:
            from hestia_earth.earth_engine import init_gee
        except ImportError:
            raise ImportError("Run `pip install hestia_earth.earth_engine` to use this functionality")

        init_gee()


def main(args):
    # print everything to console as disabled by default
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.getLevelName('DEBUG'))

    model = args[0]
    model_value = args[1]
    filepath = args[2]

    with open(filepath) as f:
        node = json.load(f)

    print(f"processing {filepath}")
    _init_gee(model)

    now = current_time_ms()
    run = importlib.import_module(f"hestia_earth.models.{model}").run
    data = run(model_value, node)
    logger.info('time=%s, unit=ms', current_time_ms() - now)

    with open(f"{filepath}-processed", 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
